use advent_of_code_rust_2021::get_day;

use std::os::raw::c_char;

#[no_mangle]
pub extern "C" fn aoc_solution(day: u32, part: u8, input: *const c_char, output: *mut u8, output_length: u32)
{
    let input_str = unsafe { std::ffi::CStr::from_ptr(input).to_str().unwrap_or("") };
    if input_str.is_empty()
    {
        return;
    }

    let (part1, part2) = get_day(day);
    let mut result = if part == 0 { part1(input_str) } else { part2(input_str) };

    if result.len() >= output_length as usize
    {
        result = "Error: output string is too small".to_string();
        assert!(result.len() < output_length as usize);
    }

    let c_result = std::ffi::CString::new(result).unwrap();
    let bytes = c_result.as_bytes_with_nul();

    let header_bytes = unsafe { std::slice::from_raw_parts_mut(output, output_length as usize) };
    header_bytes[..bytes.len()].copy_from_slice(bytes);
}