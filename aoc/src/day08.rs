use itertools::Itertools;
use std::collections::HashMap;

fn check_for_1478(line: &str) -> u32 {
    const DIGIT_LENGTHS: [usize; 4] = [2, 3, 4, 7];

    let (_, output_str) = line.split(" | ").collect_tuple().unwrap();
    output_str.split_whitespace().filter(|digit: &&str| DIGIT_LENGTHS.contains(&digit.len())).count() as u32
}

pub fn part1(input: &str) -> String {
    let result: u32 = input.lines().map(check_for_1478).sum();
    format!("{}", result)
}

fn hash_digit(digit: &str) -> u32 {
    digit.bytes().map(|b| 1 << (b - b'a')).sum::<u32>()
}

fn map_digit_1(digit: &str, digit_mapping: &mut HashMap::<u32, u32>, one: &mut String, four: &mut String) {
    assert!(!digit_mapping.contains_key(&hash_digit(digit)));

    match digit.len() {
        2 => {
            assert!(!digit_mapping.values().contains(&1));
            digit_mapping.insert(hash_digit(digit), 1);
            *one = digit.to_string();
        },
        3 => {
            assert!(!digit_mapping.values().contains(&7));
            digit_mapping.insert(hash_digit(digit), 7);
        },
        4 => {
            assert!(!digit_mapping.values().contains(&4));
            digit_mapping.insert(hash_digit(digit), 4);
            *four = digit.to_string();
        },
        7 => {
            assert!(!digit_mapping.values().contains(&8));
            digit_mapping.insert(hash_digit(digit), 8);
        },
        _ => ()
    }
}

fn map_digit_2(digit: &str, digit_mapping: &mut HashMap::<u32, u32>, one: &str, four: &str) {

    let one_count = one.chars().filter(|c| digit.chars().contains(&c)).count();
    let four_count = four.chars().filter(|c| digit.chars().contains(&c)).count();
    match digit.len() {
        // 2, 3, 5
        5 => {
            let digit_value: u32;
            if one_count == 2 {
                digit_value = 3;
            }
            else {
                assert!(one_count == 1);
                if four_count == 2 {
                    digit_value = 2;
                }
                else {
                    assert!(four_count == 3);
                    digit_value = 5;
                }
            }

            assert!(!digit_mapping.values().contains(&digit_value));
            digit_mapping.insert(hash_digit(digit), digit_value);
        },
        // 0, 6, 9
        6 => {
            let digit_value: u32;
            if four_count == 4{
                digit_value = 9;
            }
            else {
                assert!(four_count == 3);
                if one_count == 1 {
                    digit_value = 6;
                }
                else {
                    assert!(one_count == 2);
                    digit_value = 0;
                }
            }

            assert!(!digit_mapping.values().contains(&digit_value));
            digit_mapping.insert(hash_digit(digit), digit_value);
        },
        _ => ()
    }
}

fn decode_signal_entry(line: &str) -> u32 {
    let (signal_pattern, output_str) = line.split(" | ").collect_tuple().unwrap();

    let mut digit_to_value = HashMap::<u32, u32>::new();
    let mut one = String::new();
    let mut four = String::new();
    signal_pattern.split_whitespace().for_each(|digit| map_digit_1(digit, &mut digit_to_value, &mut one, &mut four));
    signal_pattern.split_whitespace().for_each(|digit| map_digit_2(digit, &mut digit_to_value, one.as_str(), four.as_str()));

    output_str
    .split_whitespace()
    .map(|digit| digit_to_value.get(&hash_digit(digit)).unwrap())
    .enumerate()
    .map(|(i, val)| val * 10_u32.pow(3 - i as u32))
    .sum::<u32>()
}

pub fn part2(input: &str) -> String {
    let result: u32 = input.lines().map(decode_signal_entry).sum();
    format!("{}", result)
}
