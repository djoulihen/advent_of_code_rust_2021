pub struct BitStream
{
    storage: Vec<u8>,
    bit_size: usize,
}

pub struct BitIterator<'a>
{
    stream: &'a BitStream,
    index: usize,
}

impl<'a> Iterator for BitIterator<'a>
{
    type Item = u8;

    fn next(&mut self) -> Option<Self::Item>
    {
        if self.index >= self.stream.bit_size
        {
            return None;
        }

        let bit_storage_index = self.index / 8;
        let bit_storage_offset = 7 - (self.index % 8);
        assert!(bit_storage_index < self.stream.storage.len());
        let mut result = self.stream.storage[bit_storage_index];
        result = (result >> bit_storage_offset) & 0b1;
        self.index += 1;
        Some(result)
    }
}

impl<'a> BitIterator<'a>
{
    pub fn concat_next(&mut self, count: usize) -> Option<u32>
    {
        if count > 32 || self.index + count > self.stream.bit_size
        {
            return None;
        }

        let mut result = 0;
        for i in 0..count
        {
            if let Some(bit) = self.next()
            {
                result |= (bit as u32) << (count - i - 1);
            }
            else
            {
                unreachable!();
            }
        }

        Some(result)
    }

    pub fn index(&self) -> usize { self.index }
}

impl BitStream
{
    pub fn iter(&self) -> BitIterator
    {
        BitIterator{ stream: self, index: 0 }
    }

    pub fn from_hex_string(input: &str) -> Self
    {
        let storage_size = input.len() / 2 + input.len() % 2;
        let mut storage = vec![0; storage_size];
        for (i, b) in input.bytes().enumerate()
        {
            let storage_index = i / 2;
            let mut int_value = match b
            {
                b'A'..=b'F' => b - b'A' + 10,
                b'0'..=b'9' => b - b'0',
                _ => unreachable!()
            };
            if i % 2 == 0 {
                int_value <<= 4;
            }
            storage[storage_index] |= int_value;
        }

        Self{ storage, bit_size: input.len() * 4 }
    }
}

impl std::fmt::Debug for BitStream
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        self.storage.iter().try_for_each(|v| write!(f, "{:08b}", v))
    }
}