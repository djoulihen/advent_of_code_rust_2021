use crate::math::{Matrix3D, Vec3D};

use rayon::prelude::*;

use itertools::Itertools;

use std::collections::{HashSet, HashMap};
use std::str::Lines;

////////////////////////////////////////// Matrix helpers //////////////////////////////////////////

pub fn identity() -> Matrix3D<i32>
{
    let mut identity = Matrix3D::default();
    for i in 0..3
    {
        identity[i][i] = 1;
    }
    identity
}

/////////////////////////////////////// Scanners and Beacons ///////////////////////////////////////

struct Beacon
{
    pos: Vec3D<i32>,
    abs_pos: Option<Vec3D<i32>>,
}

impl Beacon
{
    fn new(pos: Vec3D<i32>) -> Self
    {
        Beacon{ pos, abs_pos: None }
    }
}

impl std::fmt::Debug for Beacon
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "{:?} -> {:?}", self.pos, self.abs_pos)
    }
}

struct Scanner
{
    beacons: Vec<Beacon>,
    overlapping_scanners: Vec<(usize, Matrix3D<i32>, Vec3D<i32>)>,
    abs_rotation: Option<Matrix3D<i32>>,
    abs_translation: Option<Vec3D<i32>>
}

impl std::fmt::Debug for Scanner
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "beacons:\n")?;
        self.beacons.iter().try_for_each(|v| write!(f, "{:?}\n", v))?;
        write!(f, "overlapping scanners: {:?}", self.overlapping_scanners)
    }
}

impl Scanner
{
    fn new(beacons: Vec<Beacon>) -> Self
    {
        Scanner{ beacons, overlapping_scanners: Vec::new(), abs_rotation: None, abs_translation: None }
    }

    fn get_coordinate_rotations() -> Vec<Matrix3D<i32>>
    {
        let all_rotations = Vec::from([
            Matrix3D::from([
                [1, 0, 0],
                [0, 1, 0],
                [0, 0, 1],
            ]),
            Matrix3D::from([
                [1, 0, 0],
                [0, 0, -1],
                [0, 1, 0],
            ]),
            Matrix3D::from([
                [1, 0, 0],
                [0, -1, 0],
                [0, 0, -1],
            ]),
            Matrix3D::from([
                [1, 0, 0],
                [0, 0, 1],
                [0, -1, 0],
            ]),
            Matrix3D::from([
                [0, -1, 0],
                [1, 0, 0],
                [0, 0, 1],
            ]),
            Matrix3D::from([
                [0, 0, 1],
                [1, 0, 0],
                [0, 1, 0],
            ]),
            Matrix3D::from([
                [0, 1, 0],
                [1, 0, 0],
                [0, 0, -1],
            ]),
            Matrix3D::from([
                [0, 0, -1],
                [1, 0, 0],
                [0, -1, 0],
            ]),
            Matrix3D::from([
                [-1, 0, 0],
                [0, -1, 0],
                [0, 0, 1],
            ]),
            Matrix3D::from([
                [-1, 0, 0],
                [0, 0, -1],
                [0, -1, 0],
            ]),
            Matrix3D::from([
                [-1, 0, 0],
                [0, 1, 0],
                [0, 0, -1],
            ]),
            Matrix3D::from([
                [-1, 0, 0],
                [0, 0, 1],
                [0, 1, 0],
            ]),
            Matrix3D::from([
                [0, 1, 0],
                [-1, 0, 0],
                [0, 0, 1],
            ]),
            Matrix3D::from([
                [0, 0, 1],
                [-1, 0, 0],
                [0, -1, 0],
            ]),
            Matrix3D::from([
                [0, -1, 0],
                [-1, 0, 0],
                [0, 0, -1],
            ]),
            Matrix3D::from([
                [0, 0, -1],
                [-1, 0, 0],
                [0, 1, 0],
            ]),
            Matrix3D::from([
                [0, 0, -1],
                [0, 1, 0],
                [1, 0, 0],
            ]),
            Matrix3D::from([
                [0, 1, 0],
                [0, 0, 1],
                [1, 0, 0],
            ]),
            Matrix3D::from([
                [0, 0, 1],
                [0, -1, 0],
                [1, 0, 0],
            ]),
            Matrix3D::from([
                [0, -1, 0],
                [0, 0, -1],
                [1, 0, 0],
            ]),
            Matrix3D::from([
                [0, 0, -1],
                [0, -1, 0],
                [-1, 0, 0],
            ]),
            Matrix3D::from([
                [0, -1, 0],
                [0, 0, 1],
                [-1, 0, 0],
            ]),
            Matrix3D::from([
                [0, 0, 1],
                [0, 1, 0],
                [-1, 0, 0],
            ]),
            Matrix3D::from([
                [0, 1, 0],
                [0, 0, -1],
                [-1, 0, 0],
            ]),
        ]);
        all_rotations
    }
}

////////////////////////////////////////// Parsing //////////////////////////////////////////

fn parse_beacon(line: &str) -> Beacon
{
    let (x, y, z) = line.split(',').map(|x| x.parse::<i32>().unwrap()).collect_tuple().unwrap();
    Beacon::new(Vec3D::new(x, y, z))
}

struct Scanners<'a>
{
    lines: Lines<'a>,
}

impl<'a> Iterator for Scanners<'a>
{
    type Item = Scanner;

    fn next(&mut self) -> Option<Self::Item>
    {
        if let Some(line) = self.lines.next()
        {
            assert!(line.starts_with("--- scanner "));
        }
        else
        {
            return None;
        }

        let beacons = self.lines.by_ref()
            .take_while(|line| !line.is_empty())
            .map(parse_beacon)
            .collect();
        Some(Scanner::new(beacons))
    }
}

fn scanners(input: &str) -> Scanners
{
    Scanners{ lines: input.lines() }
}

////////////////////////////////////////// Processing /////////////////////////////////////////////

fn try_overlap_with_rotation(scanner1: &Scanner, scanner2: &Scanner, rotation: &Matrix3D<i32>) -> Option<Vec3D<i32>>
{
    let mut translations = HashMap::new();
    let beacon1_count = scanner1.beacons.len();
    let mut max_count = 0;
    for (beacon1_idx, beacon1) in scanner1.beacons.iter().enumerate()
    {
        for beacon2 in &scanner2.beacons
        {
            let beacon2_transformed = rotation * &beacon2.pos;
            let translation = &beacon2_transformed - &beacon1.pos;
            let current_count = translations.entry(translation).or_insert(0);
            *current_count += 1;

            if *current_count >= 12
            {
                return Some(translation);
            }

            max_count = std::cmp::max(max_count, *current_count);
        }

        if max_count + (beacon1_count - beacon1_idx - 1) < 12
        {
            return None;
        }
    }

    None
}

fn try_overlap(scanner1: &Scanner, scanner2: &Scanner, all_rotations: &Vec<Matrix3D<i32>>) -> Option<(Matrix3D<i32>, Vec3D<i32>)>
{
    for rotation in all_rotations
    {
        if let Some(translation) = try_overlap_with_rotation(scanner1, scanner2, &rotation)
        {
            return Some((*rotation, translation));
        }
    }
    None
}

fn resolve_absolute_positions(scanners: &mut Vec<Scanner>)
{
    let mut processed = vec![false; scanners.len()];

    processed[0] = true;
    for beacon in &mut scanners[0].beacons
    {
        beacon.abs_pos = Some(beacon.pos);
    }
    scanners[0].abs_rotation = Some(identity());
    scanners[0].abs_translation = Some(Vec3D::default());

    let mut stack = Vec::new();
    stack.push(0);
    while let Some(index) = stack.pop()
    {
        let abs_rotation = scanners[index].abs_rotation.unwrap();
        let abs_translation = scanners[index].abs_translation.unwrap();

        for overlapping_scanner_index in 0..scanners[index].overlapping_scanners.len()
        {
            let (overlapping_scanner, rotation, translation) = scanners[index].overlapping_scanners[overlapping_scanner_index];
            if processed[overlapping_scanner]
            {
                continue;
            }

            let scanner_abs_rotation = &abs_rotation * &rotation;
            let scanner_abs_translation = &(&abs_rotation * &translation) + &abs_translation;

            for beacon2 in &mut scanners[overlapping_scanner].beacons
            {
                let abs_pos = &(&scanner_abs_rotation * &beacon2.pos) - &scanner_abs_translation;
                beacon2.abs_pos = Some(abs_pos);
            }

            processed[overlapping_scanner] = true;
            scanners[overlapping_scanner].abs_rotation = Some(scanner_abs_rotation);
            scanners[overlapping_scanner].abs_translation = Some(scanner_abs_translation);
            stack.push(overlapping_scanner);
        }
    }

    assert!(processed.iter().all(|x| *x));
}

fn locate_beacons(scanners: &mut Vec<Scanner>)
{
    // let threadpool = ThreadPool::builder().pool_size(num_cpus::get() + 1);

    let all_rotations = Scanner::get_coordinate_rotations();
    let scanner_count = scanners.len();

    let results = (0..scanner_count).into_par_iter().flat_map(|i|
        {
            let local_scanners = &scanners;
            let rotations = &all_rotations;
            ((i+1)..scanner_count).into_par_iter().map(move |j|
                (i, j, try_overlap(&local_scanners[i], &local_scanners[j], rotations))
            )
        }
    ).collect::<Vec<_>>();

    for (i, j, result) in results
    {
        if let Some((rotation, translation)) = result
        {
            let inverse_rotation = rotation.transpose();
            scanners[i].overlapping_scanners.push((j, rotation, translation));
            scanners[j].overlapping_scanners.push((i, inverse_rotation, - &(&inverse_rotation * &translation)));
        }
    }

    resolve_absolute_positions(scanners);
}

fn count_unique_beacons(scanners: &Vec<Scanner>) -> usize
{
    scanners
        .iter()
        .flat_map(|scanner| scanner.beacons.iter())
        .map(|beacon| beacon.abs_pos.unwrap())
        .collect::<HashSet<_>>()
        .len() as usize
}

fn manhattan_distance(vec1: &Vec3D<i32>, vec2: &Vec3D<i32>) -> u32
{
    (0..3).map(|i| (vec1[i] - vec2[i]).abs() as u32).sum()
}

fn max_manhattan_distance(scanners: &Vec<Scanner>) -> u32
{
    let mut max_dist = 0_u32;
    for i in 0..scanners.len()
    {
        for j in i..scanners.len()
        {
            let dist = manhattan_distance(&scanners[i].abs_translation.unwrap(), &scanners[j].abs_translation.unwrap());
            max_dist = std::cmp::max(dist, max_dist);
        }
    }
    max_dist
}

////////////////////////////////////////// Solutions /////////////////////////////////////////////

pub fn part1(input: &str) -> String
{
    let mut scanners = scanners(input).collect::<Vec<_>>();

    locate_beacons(&mut scanners);

    let beacon_count = count_unique_beacons(&scanners);
    format!("{}", beacon_count)
}

pub fn part2(input: &str) -> String
{
    let mut scanners = scanners(input).collect::<Vec<_>>();

    locate_beacons(&mut scanners);

    let max_dist = max_manhattan_distance(&scanners);
    format!("{}", max_dist)
}
