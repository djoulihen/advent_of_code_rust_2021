pub fn part1(input: &str) -> String {
    let values: Vec<i32> = input.lines().map(|x| x.parse::<i32>().unwrap()).collect();
    let increase_count = values[..values.len()-1].iter().zip(values[1..].iter()).filter(|(prev, next)| next > prev).count();
    format!("{}", increase_count)
}

pub fn part2(input: &str) -> String {
    let increase_count = input.lines().map(|x| x.parse::<i32>().unwrap()).collect::<Vec<i32>>().windows(3).map(|x| x.iter().sum()).collect::<Vec<i32>>().windows(2).filter(|x| x[1] > x[0]).count();
    format!("{}", increase_count)
}