use std::collections::HashMap;

use itertools::Itertools;

const GRID_SIZE: usize = 100;

type LavaGrid = [[u8; GRID_SIZE]; GRID_SIZE];

fn parse_grid(input: &str) -> LavaGrid {
    let mut grid: LavaGrid = [[0; GRID_SIZE]; GRID_SIZE];
    input.lines().enumerate().for_each(|(i, line)|
        line.chars().enumerate().for_each(|(j, c)| grid[i][j] = c.to_digit(10).unwrap() as u8)
    );
    grid
}

fn low_points_risk(grid: &LavaGrid) -> u32 {
    let mut total_risk = 0_u32;
    for i in 0..GRID_SIZE {
        for j in 0..GRID_SIZE {
            let val = grid[i][j];
            if i > 0 && val >= grid[i-1][j] {
                continue;
            }

            if j > 0 && val >= grid[i][j-1] {
                continue;
            }

            if i < GRID_SIZE - 1 && val >= grid[i+1][j] {
                continue;
            }

            if j < GRID_SIZE - 1 && val >= grid[i][j+1] {
                continue;
            }

            total_risk += (val as u32) + 1;
        }
    }
    total_risk
}

pub fn part1(input: &str) -> String {
    let grid = parse_grid(&input);
    let risk = low_points_risk(&grid);
    format!("{}", risk)
}

fn get_final_basin(basin: u32, basin_merges: &HashMap<u32, u32>) -> u32{
    let mut final_basin = basin;
    while let Some(merged_basin) = basin_merges.get(&final_basin) {
        final_basin = *merged_basin;
    }
    final_basin
}

fn top_basins_size(grid: &LavaGrid) -> u32 {
    let mut basins = [[0_u32; GRID_SIZE]; GRID_SIZE];
    let mut basin_sizes = HashMap::new();
    let mut basin_merges = HashMap::new();

    let mut basin_counter: u32 = 0;
    for i in 0..GRID_SIZE {
        for j in 0..GRID_SIZE {
            if grid[i][j] == 9 {
                continue;
            }

            if i > 0 && basins[i-1][j] > 0 {
                let final_neighbor_basin = get_final_basin(basins[i-1][j], &basin_merges);
                basins[i][j] = final_neighbor_basin;
                *basin_sizes.get_mut(&final_neighbor_basin).unwrap() += 1;
            }

            if j > 0 && basins[i][j-1] > 0 {
                let final_neighbor_basin = get_final_basin(basins[i][j-1], &basin_merges);

                if basins[i][j] == 0 {
                    basins[i][j] = final_neighbor_basin;
                    *basin_sizes.get_mut(&final_neighbor_basin).unwrap() += 1;
                }
                else if basins[i][j] != final_neighbor_basin {
                    basin_merges.insert(basins[i][j], basins[i][j-1]);
                    *basin_sizes.get_mut(&basins[i][j-1]).unwrap() += basin_sizes[&basins[i][j]];
                    basin_sizes.remove(&basins[i][j]);
                }
            }

            if basins[i][j] == 0 {
                basin_counter += 1;
                basins[i][j] = basin_counter;
                basin_sizes.insert(basins[i][j], 1);
            }
        }
    }

    basin_sizes.values().sorted().rev().take(3).product()
}

pub fn part2(input: &str) -> String {
    let grid = parse_grid(&input);
    let result = top_basins_size(&grid);
    format!("{}", result)
}
