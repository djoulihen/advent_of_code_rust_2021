use crate::grid::{Grid, GridPosition, HeapGrid, StackGrid};

use itertools::Itertools;
// use priority_queue::PriorityQueue;

use std::cmp::Reverse;
use std::collections::BinaryHeap;

const GRID_SIZE: usize = 100;

#[derive(Copy, Clone, Ord, Eq, PartialEq, PartialOrd)]
struct PathData
{
    shortest_dist: u32,
    visited: bool,
}

impl Default for PathData
{
    fn default() -> Self { Self{ visited: false, shortest_dist: u32::MAX } }
}

fn shortest_path_cost(
    cave: &(impl Grid<u8> + std::ops::Index<GridPosition, Output = u8>),
    cave_path_data: &mut (impl Grid<PathData> + std::ops::IndexMut<GridPosition, Output = PathData>),
    start: GridPosition,
    end: GridPosition
) -> u32
{
    cave_path_data[start].shortest_dist = 0;
    // let mut priority_queue = PriorityQueue::with_capacity(cave_path_data.len());
    let mut priority_queue = BinaryHeap::with_capacity(cave_path_data.len());
    for pos in cave.positions()
    {
        // priority_queue.push(pos, Reverse(cave_path_data[pos].shortest_dist));
        priority_queue.push(Reverse((cave_path_data[pos], pos)));
    }

    // while let Some((pos, Reverse(dist))) = priority_queue.pop()
    while let Some(Reverse((PathData{ shortest_dist: dist, visited: _}, pos))) = priority_queue.pop()
    {
        assert_eq!(cave_path_data[pos].shortest_dist, dist);

        if cave_path_data[end].shortest_dist <= dist
        {
            break;
        }

        for neighbor_pos in cave.neighbor_positions(pos, false)
        {
            if cave_path_data[neighbor_pos].visited {
                continue;
            }

            let neighbor_dist = cave[neighbor_pos] as u32;
            let dist_from_start = dist + neighbor_dist;
            if dist_from_start < cave_path_data[neighbor_pos].shortest_dist
            {
                cave_path_data[neighbor_pos].shortest_dist = dist_from_start;
                // priority_queue.change_priority(&neighbor_pos, Reverse(dist_from_start));
                priority_queue.push(Reverse((cave_path_data[neighbor_pos], neighbor_pos)));
            }
        }

        cave_path_data[pos].visited = true;
    }

    cave_path_data[end].shortest_dist
}

pub fn part1(input: &str) -> String
{
    let cave = StackGrid::<u8, GRID_SIZE, GRID_SIZE>::from_digits(input);
    let start = GridPosition{x: 0, y: 0};
    let end = GridPosition{x: (GRID_SIZE - 1) as isize, y: (GRID_SIZE - 1) as isize};
    let mut cave_path_data = StackGrid::<PathData, GRID_SIZE, GRID_SIZE>::new();
    let path_cost = shortest_path_cost(&cave, &mut cave_path_data, start, end);
    format!("{}", path_cost)
}

pub fn part2(input: &str) -> String
{
    const TILED_GRID_SIZE: usize = 5 * GRID_SIZE;

    let cave = StackGrid::<u8, GRID_SIZE, GRID_SIZE>::from_digits(input);
    let mut tiled_cave = HeapGrid::<u8>::new(TILED_GRID_SIZE, TILED_GRID_SIZE);

    for (tile_x, tile_y) in (0..5).cartesian_product(0..5)
    {
        let offset = tile_x + tile_y;
        for pos in cave.positions()
        {
            let mut val = cave[pos];
            val += offset;
            val = val % 10 + val / 10;
            let tile_pos = GridPosition
            {
                x: (tile_x as usize * GRID_SIZE) as isize + pos.x,
                y: (tile_y as usize * GRID_SIZE) as isize + pos.y,
            };
            tiled_cave[tile_pos] = val;
        }
    }

    let start = GridPosition{x: 0, y: 0};
    let end = GridPosition{x: (TILED_GRID_SIZE - 1) as isize, y: (TILED_GRID_SIZE - 1) as isize};
    let mut cave_path_data = HeapGrid::<PathData>::new(TILED_GRID_SIZE, TILED_GRID_SIZE);
    let path_cost = shortest_path_cost(&tiled_cave, &mut cave_path_data, start, end);
    format!("{}", path_cost)
}
