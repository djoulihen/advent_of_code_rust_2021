const REPORT_SIZE: usize = 12;

pub fn part1(input: &str) -> String {
    let mut counters: [i32; REPORT_SIZE] = [0; REPORT_SIZE];
    let mut line_count = 0;
    input.lines().for_each(|line| {
        line.chars().enumerate().for_each(|(i, binary_value)|
            counters[i] += if binary_value == '1' { 1 } else { 0 }
        );
        line_count += 1;
    });

    let gamma_rate: i32 = counters
        .into_iter()
        .enumerate()
        .filter(|(_, count)| *count > line_count - *count)
        .map(|(i, _)| 1 << REPORT_SIZE - 1 - i).sum();
    let mask = (1 << REPORT_SIZE) - 1;
    let epsilon_rate = !gamma_rate & mask;

    format!("{}", gamma_rate * epsilon_rate)
}

fn get_rating(selected_values: &mut Vec<i32>, next_selected_values: &mut Vec<i32>, use_most_common: bool) -> i32 {

    let mut current_column_index = 0;
    while selected_values.len() > 1 {
        assert!(next_selected_values.is_empty());
        assert!(current_column_index < REPORT_SIZE);

        let mut bit_count = 0;
        for val in &(*selected_values) {
            if (*val & (1 << REPORT_SIZE - 1 - current_column_index)) != 0 {
                bit_count += 1;
            }
        }

        let select_set_bits: bool;
        if bit_count >= selected_values.len() - bit_count {
            select_set_bits = use_most_common;
        }
        else {
            select_set_bits = !use_most_common;
        }

        for val in &(*selected_values) {
            if ((*val & (1 << REPORT_SIZE - 1 - current_column_index)) != 0) == select_set_bits {
                next_selected_values.push(*val);
            }
        }

        std::mem::swap(selected_values, next_selected_values);
        next_selected_values.clear();

        current_column_index += 1;
    }

    assert!(!selected_values.is_empty());
    selected_values[0]
}

pub fn part2(input: &str) -> String {
    let decimal_values: Vec<i32> = input.lines().map(
        |line| line.chars().enumerate().map(|(i, c)| if c == '1' { 1 << (REPORT_SIZE - 1 - i) } else { 0 }).sum()
    ).collect();

    let mut selected_values = decimal_values.clone();
    let mut next_selected_values = Vec::<i32>::with_capacity(decimal_values.len());

    let rating_o2 = get_rating(&mut selected_values, &mut next_selected_values, true);

    selected_values.extend_from_slice(&decimal_values);
    next_selected_values.clear();
    let rating_co2 = get_rating(&mut selected_values, &mut next_selected_values, false);

    format!("{}", rating_o2 * rating_co2)
}