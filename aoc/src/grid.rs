use std::iter::Iterator;

pub struct GridNeighborPositions
{
    grid_size: Option<(usize, usize)>,
    pos: GridPosition,
    use_diagonals: bool,
    index: u32,
}

impl Iterator for GridNeighborPositions
{
    type Item = GridPosition;

    fn next(&mut self) -> Option<Self::Item>
    {
        let neighbor_count = if self.use_diagonals { 8 } else { 4 };
        while self.index < neighbor_count
        {
            let (dx, dy) = match self.index
            {
                0 => if self.use_diagonals { (-1, -1) } else { (0, -1) },
                1 => if self.use_diagonals { (0, -1) } else { (-1, 0) },
                2 => if self.use_diagonals { (1, -1) } else { (1, 0) },
                3 => if self.use_diagonals { (-1, 0) } else { (0, 1) },
                4 => (1, 0),
                5 => (-1, 1),
                6 => (0, 1),
                7 => (1, 1),
                _ => unreachable!()
            };

            self.index += 1;

            let neighbor_x = self.pos.x as i64 + dx;
            let neighbor_y = self.pos.y as i64 + dy;
            match self.grid_size
            {
                Some((grid_width, grid_height)) =>
                {
                    if neighbor_x >= 0 && neighbor_x < grid_width as i64 && 
                        neighbor_y >= 0 && neighbor_y < grid_height as i64
                    {
                        return Some(GridPosition{ x: neighbor_x as isize, y: neighbor_y as isize});
                    }
                },
                None => { return Some(GridPosition{ x: neighbor_x as isize, y: neighbor_y as isize}); }
            }
        }

        return None;
    }
}

#[derive(Copy, Clone, Hash, Eq, PartialEq, Ord, PartialOrd)]
pub struct GridPosition
{
    pub x: isize,
    pub y: isize
}

impl GridPosition
{
    #[allow(dead_code)]
    pub fn neighbors(&self, use_diagonals: bool) -> GridNeighborPositions
    {
        GridNeighborPositions{grid_size: None, pos: *self, use_diagonals, index: 0}
    }

    pub fn kernel(&self, width: usize, height: usize) -> impl Iterator<Item = GridPosition>
    {
        let mut index = 0;
        let half_width = (width / 2) as isize;
        let half_height = (height / 2) as isize;
        let pos_x = self.x;
        let pos_y = self.y;

        std::iter::from_fn(move || {
            if index < width * height
            {
                let x = (index % width) as isize;
                let y = (index / width) as isize;
                let kernel_pos_x = pos_x - half_width + x;
                let kernel_pos_y = pos_y - half_height + y;
                index += 1;
                return Some(GridPosition{x: kernel_pos_x, y: kernel_pos_y});
            }

            None
        })
    }
}

pub struct GridPositions
{
    grid_width: usize,
    grid_height: usize,
    index: usize,
}

impl Iterator for GridPositions
{
    type Item = GridPosition;

    fn next(&mut self) -> Option<Self::Item>
    {
        if self.index >= self.grid_width * self.grid_height
        {
            return None;
        }

        let pos = GridPosition
        {
            x: (self.index % self.grid_width) as isize,
            y: (self.index / self.grid_width) as isize
        };
        self.index += 1;
        Some(pos)
    }
}

pub trait Grid<T: Sized>
{
    fn width(&self) -> usize;
    fn height(&self) -> usize;
    fn len(&self) -> usize { self.width() * self.height() }

    fn neighbor_positions(&self, pos: GridPosition, use_diagonals: bool) -> GridNeighborPositions
    {
        GridNeighborPositions{grid_size: Some((self.width(), self.height())), pos, use_diagonals, index: 0}
    }

    fn positions(&self) -> GridPositions
    {
        GridPositions{grid_width: self.width(), grid_height: self.height(), index: 0}
    }

    fn is_inside(&self, pos: GridPosition) -> bool
    {
        pos.x >= 0 && pos.y >= 0 && (pos.x as usize) < self.width() && (pos.y as usize) < self.height()
    }
}

pub struct StackGrid<T: Sized, const WIDTH: usize, const HEIGHT: usize>
{
    storage: [[T; WIDTH]; HEIGHT]
}

impl<T, const WIDTH: usize, const HEIGHT: usize> StackGrid<T, WIDTH, HEIGHT>
{
    #[allow(dead_code)]
    pub fn values(&self) -> impl Iterator<Item = &T>
    {
        self.storage.iter().flat_map(|row| row.iter())
    }
}

impl<T, const WIDTH: usize, const HEIGHT: usize> StackGrid<T, WIDTH, HEIGHT>
    where T: Default + Copy
{
    pub fn new() -> Self
    {
        Self{ storage: [[T::default(); WIDTH]; HEIGHT] }
    }
}

impl<T, const WIDTH: usize, const HEIGHT: usize> Grid<T> for StackGrid<T, WIDTH, HEIGHT>
{
    fn width(&self) -> usize { WIDTH }
    fn height(&self) -> usize { HEIGHT }
}

impl<T, const WIDTH: usize, const HEIGHT: usize> std::ops::Index<GridPosition> for StackGrid<T, WIDTH, HEIGHT>
{
    type Output = T;

    fn index(&self, pos: GridPosition) -> &Self::Output
    {
        &self.storage[pos.y as usize][pos.x as usize]
    }
}

impl<T, const WIDTH: usize, const HEIGHT: usize> std::ops::IndexMut<GridPosition> for StackGrid<T, WIDTH, HEIGHT>
{
    fn index_mut(&mut self, pos: GridPosition) -> &mut Self::Output
    {
        &mut self.storage[pos.y as usize][pos.x as usize]
    }
}

impl<T, const WIDTH: usize, const HEIGHT: usize> std::fmt::Debug for StackGrid<T, WIDTH, HEIGHT>
    where T: std::fmt::Debug
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "{:?}", self.storage)
    }
}

impl<const WIDTH: usize, const HEIGHT: usize> StackGrid<u8, WIDTH, HEIGHT>
{
    pub fn from_digits(input: &str) -> Self
    {
        let mut grid = Self::new();
        input.lines().enumerate().for_each(|(y, line)|
            line.chars().enumerate().for_each(
                |(x, c)| grid[GridPosition{x: x as isize, y: y as isize}] = c.to_digit(10).unwrap() as u8
            )
        );
        grid
    }
}

pub struct HeapGrid<T: Sized>
{
    storage: Vec<T>,
    width: usize,
    height: usize,
}

impl<T> HeapGrid<T>
{
    pub fn values(&self) -> impl Iterator<Item = &T>
    {
        self.storage.iter()
    }

    pub fn values_mut(&mut self) -> impl Iterator<Item = &mut T>
    {
        self.storage.iter_mut()
    }
}

impl<T> HeapGrid<T>
    where T: Default + Clone
{
    pub fn new(width: usize, height: usize) -> Self
    {
        Self{ storage: vec![T::default(); width * height], width, height}
    }
}

impl<T> Grid<T> for HeapGrid<T>
{
    fn width(&self) -> usize { self.width }
    fn height(&self) -> usize { self.height }
}

impl<T> std::ops::Index<GridPosition> for HeapGrid<T>
{
    type Output = T;

    fn index(&self, pos: GridPosition) -> &Self::Output
    {
        &self.storage[pos.y as usize * self.width + pos.x as usize]
    }
}

impl<T> std::ops::IndexMut<GridPosition> for HeapGrid<T>
{
    fn index_mut(&mut self, pos: GridPosition) -> &mut Self::Output
    {
        &mut self.storage[pos.y as usize * self.width + pos.x as usize]
    }
}

impl<T> std::fmt::Debug for HeapGrid<T>
    where T: std::fmt::Debug
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "{:?}", self.storage)
    }
}