#[derive(Default, Debug)]
struct TargetArea
{
    x_min: i32,
    x_max: i32,
    y_min: i32,
    y_max: i32,
}

pub fn part1(input: &str) -> String {
    let mut target_area = TargetArea::default();
    text_io::scan!(
        input.bytes() => "target area: x={}..{}, y={}..{}",
        target_area.x_min,
        target_area.x_max,
        target_area.y_min,
        target_area.y_max
    );

    let mut max_y = 0;
    for vx in 1..target_area.x_max
    {
        let x_max = vx * vx - vx * (vx - 1) / 2;
        if x_max < target_area.x_min
        {
            continue;
        }

        for vy in 1..1000
        {
            let y_max = vy * vy - vy * (vy - 1) / 2;
            let mut x = 0;
            let mut y = 0;
            let mut current_vx = vx;
            let mut current_vy = vy;
            loop
            {
                x += current_vx;
                y += current_vy;
                if current_vx > 0 { current_vx -= 1; }
                current_vy -= 1;

                if x > target_area.x_max || (current_vy < 0 && y < target_area.y_min)
                {
                    break;
                }

                // let x = std::cmp::min(x_max, step * vx - step * (step - 1) / 2);
                // let y = step * vy - step * (step - 1) / 2;
                if x >= target_area.x_min && x <= target_area.x_max && y >= target_area.y_min && y <= target_area.y_max && y_max > max_y
                {
                    max_y = y_max;
                    break;
                }
            }
        }
    }

    format!("{}", max_y)
}

pub fn part2(input: &str) -> String {
    let mut target_area = TargetArea::default();
    text_io::scan!(
        input.bytes() => "target area: x={}..{}, y={}..{}",
        target_area.x_min,
        target_area.x_max,
        target_area.y_min,
        target_area.y_max
    );

    let mut count = 0;
    for vx in 1..target_area.x_max+1
    {
        let x_max = vx * vx - vx * (vx - 1) / 2;
        if x_max < target_area.x_min
        {
            continue;
        }

        for vy in target_area.y_min..1000
        {
            let mut x = 0;
            let mut y = 0;
            let mut current_vx = vx;
            let mut current_vy = vy;
            loop
            {
                x += current_vx;
                y += current_vy;
                if current_vx > 0 { current_vx -= 1; }
                current_vy -= 1;

                if x > target_area.x_max || (current_vy < 0 && y < target_area.y_min)
                {
                    break;
                }

                // let x = std::cmp::min(x_max, step * vx - step * (step - 1) / 2);
                // let y = step * vy - step * (step - 1) / 2;
                if x >= target_area.x_min && y >= target_area.y_min && y <= target_area.y_max
                {
                    count += 1;
                    break;
                }
            }
        }
    }

    format!("{}", count)
}
