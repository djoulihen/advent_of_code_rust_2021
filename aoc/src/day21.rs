use std::collections::HashMap;

use itertools::Itertools;

struct DeterministicDie
{
    roll_count: u32,
}

impl DeterministicDie
{
    fn new() -> Self { Self{ roll_count: 0 } }

    fn roll_thrice(&mut self) -> u32
    {
        let result = 3 * (self.roll_count + 1) + 3;
        self.roll_count += 3;
        result
    }
}

fn parse_start_positions(input: &str) -> (u8, u8)
{
    let p1_start: u8;
    let p2_start: u8;
    let mut lines = input.lines();
    text_io::scan!(lines.next().unwrap().bytes() => "Player 1 starting position: {}", p1_start);
    text_io::scan!(lines.next().unwrap().bytes() => "Player 2 starting position: {}", p2_start);
    (p1_start, p2_start)
}

pub fn part1(input: &str) -> String {
    let start_positions = parse_start_positions(input);
    let mut die = DeterministicDie::new();

    let mut scores = (0, 0);
    let mut positions = (start_positions.0 as u32, start_positions.1 as u32);
    let mut player_turn = 0;
    while scores.0 < 1000 && scores.1 < 1000
    {
        let (position, score) = if player_turn == 0 { (&mut positions.0, &mut scores.0) } else { (&mut positions.1, &mut scores.1) };
        let roll = die.roll_thrice();
        *position = ((*position + roll - 1) % 10) + 1;
        *score += *position;
        player_turn = (player_turn + 1) % 2;
    }
    let result = std::cmp::min(scores.0, scores.1) * die.roll_count;
    format!("{}", result)
}

fn compute_wins(positions: (u8, u8), scores: (u8, u8), player: u8, cache: &mut HashMap<((u8, u8), (u8, u8), u8), (u64, u64)>) -> (u64, u64)
{
    let cache_key = (positions, scores, player);
    if let Some(wins) = cache.get(&cache_key)
    {
        return *wins;
    }

    let mut wins = (0, 0);
    for die_roll in (1..4).cartesian_product(1..4).cartesian_product(1..4).map(|rolls| rolls.0.0 + rolls.0.1 + rolls.1)
    {
        let mut new_positions = positions;
        let mut new_scores = scores;
        let (player_position, player_score, player_wins) =
            if player == 0 { (&mut new_positions.0, &mut new_scores.0, &mut wins.0) }
            else { (&mut new_positions.1, &mut new_scores.1, &mut wins.1) };
        *player_position = ((*player_position + die_roll - 1) % 10) + 1;
        *player_score += *player_position;

        if *player_score >= 21
        {
            *player_wins += 1;
        }
        else
        {
            let roll_wins = compute_wins(new_positions, new_scores, (player + 1) % 2, cache);
            wins.0 += roll_wins.0;
            wins.1 += roll_wins.1;
        }
    }

    cache.insert(cache_key, wins);
    wins
}

pub fn part2(input: &str) -> String {
    let start_positions = parse_start_positions(input);

    let mut cache = HashMap::new();
    let wins = compute_wins(start_positions, (0, 0), 0, &mut cache);
    println!("{}, {}", wins.0, wins.1);
    format!("{}", std::cmp::max(wins.0, wins.1))
}
