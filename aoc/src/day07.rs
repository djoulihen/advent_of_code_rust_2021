pub fn part1(input: &str) -> String {
    let positions: Vec<i32> = input.lines().next().unwrap().split(',').map(|x| x.parse::<i32>().unwrap()).collect();
    let min_pos = *positions.iter().min().unwrap();
    let max_pos = *positions.iter().max().unwrap();

    let mut min_fuel = i32::MAX;
    for pos in min_pos..max_pos+1 {
        let fuel: i32 = positions.iter().map(|x| (x - pos).abs()).sum();
        if fuel < min_fuel {
            min_fuel = fuel;
        }
    }
    format!("{}", min_fuel)
}

pub fn part2(input: &str) -> String {
    let positions: Vec<i32> = input.lines().next().unwrap().split(',').map(|x| x.parse::<i32>().unwrap()).collect();
    let min_pos = *positions.iter().min().unwrap();
    let max_pos = *positions.iter().max().unwrap();

    let min_fuel: i32 = (min_pos..max_pos+1).map(|pos|
        positions.iter().map(|x| { let dist = (x - pos).abs(); dist * (dist + 1) / 2 }).sum()
    ).min().unwrap();

    // Faster alternative, in practice should be trying both +0.5 and -0.5 and taking the best value
    // let min_pos = (positions.iter().sum::<i32>() as f32 / positions.len() as f32 - 0.5).round() as i32;
    // let min_fuel: i32 = positions.iter().map(|x| { let dist = (x - min_pos).abs(); dist * (dist + 1) / 2 }).sum();
    format!("{}", min_fuel)

}
