use std::collections::HashMap;

use itertools::Itertools;

fn line_score(line: &str) -> u32 {
    let char_scores: HashMap<char, u32> = HashMap::from([
        (')', 3),
        (']', 57),
        ('}', 1197),
        ('>', 25137),
    ]);

    let opening_to_closing: HashMap<char, char> = HashMap::from([
        ('(', ')'),
        ('[', ']'),
        ('{', '}'),
        ('<', '>'),
    ]);

    let mut delimiter_stack = Vec::<char>::new();
    let mut score = 0_u32;
    for c in line.chars() {
        if opening_to_closing.keys().contains(&c) {
            delimiter_stack.push(c);
        }
        else if opening_to_closing.values().contains(&c) {
            if delimiter_stack.is_empty() || c != *opening_to_closing.get(delimiter_stack.last().unwrap()).unwrap_or(&' ') {
                score += char_scores[&c];
                break;
            }
            else {
                delimiter_stack.pop();
            }
        }
        else {
            unreachable!();
        }
    }
    score
}

pub fn part1(input: &str) -> String {
    let score: u32 = input.lines().map(line_score).sum();
    format!("{}", score)
}

fn line_scores2(line: &str) -> Option<u64> {
    let char_scores: HashMap<char, u64> = HashMap::from([
        (')', 1),
        (']', 2),
        ('}', 3),
        ('>', 4),
    ]);

    let opening_to_closing: HashMap<char, char> = HashMap::from([
        ('(', ')'),
        ('[', ']'),
        ('{', '}'),
        ('<', '>'),
    ]);

    let mut delimiter_stack = Vec::<char>::new();
    for c in line.chars() {
        if opening_to_closing.keys().contains(&c) {
            delimiter_stack.push(c);
        }
        else if opening_to_closing.values().contains(&c) {
            if delimiter_stack.is_empty() || c != *opening_to_closing.get(delimiter_stack.last().unwrap()).unwrap_or(&' ') {
                return None;
            }
            else {
                delimiter_stack.pop();
            }
        }
        else {
            unreachable!();
        }
    }
    let mut score = 0_u64;
    delimiter_stack.iter().rev().for_each(|c| score = 5 * score + char_scores[&opening_to_closing[c]]);
    Some(score)
}

pub fn part2(input: &str) -> String {
    let mut scores: Vec<u64> = input.lines().filter_map(line_scores2).collect();
    scores.sort();
    let score = scores[scores.len() / 2];
    format!("{}", score)
}
