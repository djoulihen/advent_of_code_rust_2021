#[repr(u8)]
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
enum Amphipod
{
    Amber,
    Bronze,
    Copper,
    Desert
}

impl Amphipod
{
    fn from(c: char) -> Self
    {
        match c
        {
            'A' => Self::Amber,
            'B' => Self::Bronze,
            'C' => Self::Copper,
            'D' => Self::Desert,
            _ => unreachable!(),
        }
    }

    fn to_char(&self) -> char
    {
        match self
        {
            Self::Amber => 'A',
            Self::Bronze => 'B',
            Self::Copper => 'C',
            Self::Desert => 'D'
        }
    }

    fn room_index(&self) -> usize
    {
        match self
        {
            Self::Amber => 0,
            Self::Bronze => 1,
            Self::Copper => 2,
            Self::Desert => 3
        }
    }

    fn move_cost(&self) -> u32
    {
        match self
        {
            Self::Amber => 1,
            Self::Bronze => 10,
            Self::Copper => 100,
            Self::Desert => 1000
        }
    }
}

#[derive(Clone)]
struct Room
{
    hallway_door_index: usize,
    spaces: Vec<Option<Amphipod>>,
}

impl Room
{
    fn can_move_into(&self, amphipod: Amphipod) -> Option<usize>
    {
        let mut room_space_index = 0;
        while room_space_index < self.spaces.len() && self.spaces[room_space_index].is_none()
        {
            room_space_index += 1;
        }

        if room_space_index == 0
        {
            None
        }
        else if room_space_index == self.spaces.len()
        {
            Some(room_space_index - 1)
        }
        else
        {
            if self.spaces[room_space_index..].iter().all(|x| x.unwrap() == amphipod)
            {
                Some(room_space_index - 1)
            }
            else
            {
                None
            }
        }
    }

    fn has_movable_amphipod(&self) -> Option<(usize, Amphipod)>
    {
        let mut room_space_index = 0;
        while room_space_index < self.spaces.len() && self.spaces[room_space_index].is_none()
        {
            room_space_index += 1;
        }

        if room_space_index == self.spaces.len()
        {
            None
        }
        else
        {
            Some((room_space_index, self.spaces[room_space_index].unwrap()))
        }
    }
}

#[derive(Clone, Copy, Debug)]
struct RoomPosition(usize, usize);

#[derive(Clone, Copy, Debug)]
enum Move
{
    RoomToHallway{ room_pos: RoomPosition, hallway_pos: usize },
    HallwayToRoom{ hallway_pos: usize, room_pos: RoomPosition },
    RoomToRoom{ from_room_pos: RoomPosition, to_room_pos: RoomPosition },
}

#[derive(Clone, Copy, Debug)]
struct MoveData
{
    m: Move,
    amphipod: Amphipod,
    cost: u32,
    heuristic: u32,
}

impl MoveData
{
    fn new(m: Move, amphipod: Amphipod) -> Self
    {
        Self{m, amphipod, cost: 0, heuristic: 0}
    }
}

#[derive(Clone)]
struct Burrow
{
    hallway: Vec<Option<Amphipod>>,
    rooms: Vec<Room>,
}

impl Burrow
{
    fn from(input: &str) -> Self
    {
        let mut lines = input.lines();
        let hallway = vec![None; lines.next().unwrap().len() - 2];
        assert_eq!(lines.next().unwrap(), format!("#{:.^width$}#", "", width = hallway.len()));
        let mut rooms = Vec::new();
        for (room_idx, c) in lines.next().unwrap().chars().enumerate().filter(|(_, c)| *c != '#')
        {
            let amphipod = Amphipod::from(c);
            rooms.push(Room{ hallway_door_index: room_idx - 1, spaces: vec![Some(amphipod)]});
        }

        loop
        {
            let line = lines.next().unwrap();
            if line.chars().all(|c| c == '#' || c == ' ')
            {
                break;
            }

            for (room_idx, (i, c)) in line.chars().enumerate().filter(|(_, c)| *c != '#' && *c != ' ').enumerate()
            {
                assert_eq!(rooms[room_idx].hallway_door_index, i - 1);
                let amphipod = Amphipod::from(c);
                rooms[room_idx].spaces.push(Some(amphipod));
            }
        }

        let burrow = Self{ hallway, rooms };
        assert_eq!(format!("{:?}", burrow), input);
        burrow
    }

    fn space_to_char(option: &Option<Amphipod>) -> char
    {
        match option
        {
            None => '.',
            Some(amphipod) => amphipod.to_char()
        }
    }

    fn room_row_to_string(&self, row_idx: usize) -> String
    {
        assert!(!self.rooms.is_empty());

        let first_room_door_idx = self.rooms[0].hallway_door_index;
        let mut result = if row_idx == 0
        {
            format!("{:#^width$}", "", width = first_room_door_idx)
        }
        else
        {
            format!("{: ^width$}", "", width = first_room_door_idx)
        };

        let mut room_idx = 0;
        let mut hallway_idx = first_room_door_idx - 1;
        while room_idx < self.rooms.len()
        {
            let door_index = self.rooms[room_idx].hallway_door_index;
            assert!(door_index >= hallway_idx);
            result.push_str(format!("{:#^width$}", "", width = door_index - hallway_idx).as_str());
            result.push(Self::space_to_char(&self.rooms[room_idx].spaces[row_idx]));
            result.push('#');
            room_idx += 1;
            hallway_idx = door_index + 2;
        }

        let last_room_door_idx = self.rooms[self.rooms.len()-1].hallway_door_index;
        if row_idx == 0
        {
            result.push_str(format!("{:#^width$}", "", width = self.hallway.len() - last_room_door_idx - 1).as_str());
        }

        result.push('\n');
        result
    }

    fn is_hallway_clear(&self, from_index: usize, to_index: usize) -> bool
    {
        let (from, to) = if from_index < to_index { (from_index + 1, to_index + 1) } else { (to_index, from_index) };
        (from..to).all(|index| self.hallway[index].is_none())
    }

    fn get_available_moves(&self) -> Vec<MoveData>
    {
        let mut moves = Vec::new();

        // Check first for hallway-to-room moves. Always do that immediately if possible
        for hallway_index in 0..self.hallway.len()
        {
            if let Some(amphipod) = self.hallway[hallway_index]
            {
                let room_index = amphipod.room_index();
                let dest_room = &self.rooms[room_index];

                if !self.is_hallway_clear(hallway_index, dest_room.hallway_door_index)
                {
                    continue;
                }

                if let Some(room_space_index) = dest_room.can_move_into(amphipod)
                {
                    let best_move = MoveData::new(
                        Move::HallwayToRoom{
                            hallway_pos: hallway_index,
                            room_pos: RoomPosition(room_index, room_space_index)
                        },
                        amphipod
                    );
                    moves.push(best_move);
                    return moves;
                }
            }
        }

        // Then check for room-to-room moves. Also do that immediately if possible.
        // It does not really matter if we do hallwat-to-room or room-to-room first
        for from_room_index in 0..self.rooms.len()
        {
            let from_room = &self.rooms[from_room_index];
            if let Some((from_room_space_index, amphipod)) = from_room.has_movable_amphipod()
            {
                let to_room_index = amphipod.room_index();
                let to_room = &self.rooms[to_room_index];

                if from_room_index == to_room_index ||
                    !self.is_hallway_clear(from_room.hallway_door_index, to_room.hallway_door_index)
                {
                    continue;
                }

                if let Some(to_room_space_index) = to_room.can_move_into(amphipod)
                {
                    let best_move = MoveData::new(
                        Move::RoomToRoom{
                            from_room_pos: RoomPosition(from_room_index, from_room_space_index),
                            to_room_pos: RoomPosition(to_room_index, to_room_space_index),
                        },
                        amphipod
                    );
                    moves.push(best_move);
                    return moves;
                }
            }
        }

        // Finally check for room-to-hallway possibilities
        for from_room_index in 0..self.rooms.len()
        {
            let from_room = &self.rooms[from_room_index];
            if let Some((from_room_space_index, amphipod)) = from_room.has_movable_amphipod()
            {
                if from_room_index == amphipod.room_index() &&
                    from_room.spaces.iter().all(|occupant| match occupant {
                        None => true,
                        Some(a) => a.room_index() == from_room_index
                    })
                {
                    continue;
                }

                for (hallway_index, _) in self.hallway.iter().enumerate().filter(
                    |(hallway_index, occupant)|
                        occupant.is_none() &&
                        !self.rooms.iter().map(|room| room.hallway_door_index).any(|i| i == *hallway_index) &&
                        self.is_hallway_clear(from_room.hallway_door_index, *hallway_index)
                )
                {
                    let current_move = MoveData::new(
                        Move::RoomToHallway{
                            room_pos: RoomPosition(from_room_index, from_room_space_index),
                            hallway_pos: hallway_index
                        },
                        amphipod
                    );
                    moves.push(current_move);
                }
            }
        }

        moves
    }

    fn apply_move(&self, move_to_apply: &Move) -> Self
    {
        let mut new_burrow = self.clone();
        match move_to_apply
        {
            Move::RoomToHallway{room_pos, hallway_pos} => {
                assert!(new_burrow.hallway[*hallway_pos].is_none());
                new_burrow.hallway[*hallway_pos] = new_burrow.rooms[room_pos.0].spaces[room_pos.1];
                new_burrow.rooms[room_pos.0].spaces[room_pos.1] = None;
            },
            Move::HallwayToRoom{hallway_pos, room_pos} => {
                assert!(new_burrow.rooms[room_pos.0].spaces[room_pos.1].is_none());
                new_burrow.rooms[room_pos.0].spaces[room_pos.1] = new_burrow.hallway[*hallway_pos];
                new_burrow.hallway[*hallway_pos] = None;
            },
            Move::RoomToRoom{from_room_pos, to_room_pos} => {
                assert!(new_burrow.rooms[to_room_pos.0].spaces[to_room_pos.1].is_none());
                new_burrow.rooms[to_room_pos.0].spaces[to_room_pos.1] = new_burrow.rooms[from_room_pos.0].spaces[from_room_pos.1];
                new_burrow.rooms[from_room_pos.0].spaces[from_room_pos.1] = None;

            },
        }
        new_burrow
    }

    fn move_cost(&self, move_data: &MoveData) -> u32
    {
        match &move_data.m
        {
            Move::RoomToHallway{room_pos, hallway_pos} |
            Move::HallwayToRoom{hallway_pos, room_pos} =>
            {
                let hallway_dist = (*hallway_pos as i32 - self.rooms[room_pos.0].hallway_door_index as i32).abs() as u32;
                (room_pos.1 as u32 + 1 +  hallway_dist) * move_data.amphipod.move_cost()
            },
            Move::RoomToRoom{from_room_pos, to_room_pos} =>
            {
                let hallway_dist = (self.rooms[from_room_pos.0].hallway_door_index as i32 - self.rooms[to_room_pos.0].hallway_door_index as i32).abs() as u32;
                (from_room_pos.1 as u32 + to_room_pos.1 as u32 + 2 + hallway_dist) * move_data.amphipod.move_cost()
            },
        }
    }

    fn min_cost_heuristic(&self) -> u32
    {
        let mut total_cost = 0;
        for hallway_index in 0..self.hallway.len()
        {
            if let Some(amphipod) = self.hallway[hallway_index]
            {
                let room_index = amphipod.room_index();
                total_cost += self.move_cost(&MoveData::new(
                    Move::HallwayToRoom{
                        hallway_pos: hallway_index,
                        room_pos: RoomPosition(room_index, 0)
                    },
                    amphipod
                ));
            }
        }

        for from_room_index in 0..self.rooms.len()
        {
            let from_room = &self.rooms[from_room_index];
            for from_room_space_index in 0..from_room.spaces.len()
            {
                if let Some(amphipod) = &from_room.spaces[from_room_space_index]
                {
                    let to_room_index = amphipod.room_index();
                    total_cost += self.move_cost(&MoveData::new(
                        Move::RoomToRoom{
                            from_room_pos: RoomPosition(from_room_index, from_room_space_index),
                            to_room_pos: RoomPosition(to_room_index, 0)
                        },
                        *amphipod
                    ));
                }
            }
        }

        total_cost
    }

    fn compute_heuristic(&self, m: &Move) -> u32
    {
        let new_burrow = self.apply_move(m);
         new_burrow.min_cost_heuristic()
    }

    fn sort_moves(&self, moves: &mut Vec<MoveData>)
    {
        for move_data in moves.iter_mut()
        {
            move_data.cost = self.move_cost(move_data);
            move_data.heuristic = self.compute_heuristic(&move_data.m);
        }
        moves.sort_by(|a, b| (a.cost + a.heuristic).partial_cmp(&(b.cost + b.heuristic)).unwrap());
    }

    fn cull_moves(move_data: &mut Vec<MoveData>, start_index: usize, current_cost: u32, global_min_cost: &mut u32)
    {
        let mut index = start_index;
        while index < move_data.len()
        {
            if current_cost + move_data[index].cost + move_data[index].heuristic >= *global_min_cost
            {
                move_data.remove(index);
            }
            else
            {
                index += 1;
            }
        }
    }

    fn is_solved(&self) -> bool
    {
        self.hallway.iter().all(|occupant| occupant.is_none()) &&
            self.rooms.iter().enumerate().all(|(room_index, room)|
                room.spaces.iter().all(|occupant| occupant.unwrap().room_index() == room_index)
            )
    }

    fn solve(&self, current_cost: u32, global_min_cost: &mut u32)
    {
        // println!("Solving for:\n{:?}", self);
        if self.is_solved()
        {
            *global_min_cost = std::cmp::min(*global_min_cost, current_cost);
            // println!("Solved, cost {}\n", *global_min_cost);
            return;
        }

        let mut moves = self.get_available_moves();

        if moves.is_empty()
        {
            // println!("No solution !\n");
            return;
        }

        if moves.len() == 1
        {
            let cost = self.move_cost(&moves[0]);
            let MoveData{m: next_move, ..} = &moves[0];
            let new_burrow = self.apply_move(next_move);
            // println!("{:?}, cost={}\n{:?}", next_move, cost, new_burrow);
            new_burrow.solve(current_cost + cost, global_min_cost);
            return;
        }

        self.sort_moves(&mut moves);

        let mut move_index = 0;
        loop
        {
            Burrow::cull_moves(&mut moves, move_index, current_cost, global_min_cost);
            if move_index >= moves.len()
            {
                break;
            }

            let MoveData{m: next_move, cost, ..} = &moves[move_index];
            let new_burrow = self.apply_move(next_move);
            // println!("{:?}, cost={}\n{:?}", next_move, cost, new_burrow);
            new_burrow.solve(current_cost + cost, global_min_cost);
            move_index += 1;
        }
    }
}

impl std::fmt::Debug for Burrow
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        assert!(!self.rooms.is_empty());

        write!(f, "{:#^width$}\n", "", width = self.hallway.len() + 2)?;
        write!(f, "#{}#\n", self.hallway.iter().map(Burrow::space_to_char).collect::<String>())?;

        let room_space_count = self.rooms[0].spaces.len();
        assert!(self.rooms.iter().skip(1).all(|room| room.spaces.len() == room_space_count));
        for space_idx in 0..room_space_count
        {
            write!(f, "{}", self.room_row_to_string(space_idx))?;
        }
        let first_room_door_idx = self.rooms[0].hallway_door_index;
        let last_room_door_idx = self.rooms[self.rooms.len()-1].hallway_door_index;
        let first_space_len = first_room_door_idx;
        let wall_len = last_room_door_idx - first_room_door_idx + 3;
        write!(f, "{0: ^1$}{0:#^2$}\n", "", first_space_len, wall_len)
    }
}

pub fn part1(input: &str) -> String
{
    let burrow = Burrow::from(input);
    println!("{:?}", burrow);
    let mut global_min_cost = u32::MAX;
    burrow.solve(0, &mut global_min_cost);
    format!("{}", global_min_cost)
}

pub fn part2(input: &str) -> String {
    let mut burrow = Burrow::from(input);
    // Insert into the middle of the rooms
    // #D#C#B#A#
    // #D#B#A#C#
    burrow.rooms[0].spaces.insert(1, Some(Amphipod::Desert));
    burrow.rooms[0].spaces.insert(2, Some(Amphipod::Desert));
    burrow.rooms[1].spaces.insert(1, Some(Amphipod::Copper));
    burrow.rooms[1].spaces.insert(2, Some(Amphipod::Bronze));
    burrow.rooms[2].spaces.insert(1, Some(Amphipod::Bronze));
    burrow.rooms[2].spaces.insert(2, Some(Amphipod::Amber));
    burrow.rooms[3].spaces.insert(1, Some(Amphipod::Amber));
    burrow.rooms[3].spaces.insert(2, Some(Amphipod::Copper));
    println!("{:?}", burrow);
    let mut global_min_cost = u32::MAX;
    burrow.solve(0, &mut global_min_cost);
    format!("{}", global_min_cost)
}
