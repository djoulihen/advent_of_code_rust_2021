use crate::grid::{GridPosition, HeapGrid};

use itertools::Itertools;

const GRID_SIZE: usize = 1000;

type VentGrid = HeapGrid<i32>;

struct VentLine {
    x1: i32,
    y1: i32,
    x2: i32,
    y2: i32,
}

fn parse_line(line: &str) -> VentLine {
    let mut vent_line = VentLine{x1: 0, y1: 0, x2: 0, y2: 0};
    text_io::scan!(line.bytes() => "{},{} -> {},{}", vent_line.x1, vent_line.y1, vent_line.x2, vent_line.y2);
    vent_line
}

fn create_grid(vent_lines: &[VentLine]) -> VentGrid {
    let mut vent_grid = VentGrid::new(GRID_SIZE, GRID_SIZE);
    for vent_line in vent_lines {
        if vent_line.x1 == vent_line.x2 {
            for y in std::cmp::min(vent_line.y1, vent_line.y2)..std::cmp::max(vent_line.y1, vent_line.y2) + 1 {
                vent_grid[GridPosition{x: vent_line.x1 as isize, y: y as isize}] += 1;
            }
        }

        if vent_line.y1 == vent_line.y2 {
            for x in std::cmp::min(vent_line.x1, vent_line.x2)..std::cmp::max(vent_line.x1, vent_line.x2) + 1 {
                vent_grid[GridPosition{x: x as isize, y: vent_line.y1 as isize}] += 1;
            }
        }
    }
    vent_grid
}

pub fn part1(input: &str) -> String {
    let vent_lines: Vec<VentLine> = input.lines().map(|line| parse_line(line)).collect();
    let vent_grid = create_grid(&vent_lines);
    let dangerous_point_count = (0..GRID_SIZE).cartesian_product(0..GRID_SIZE).filter(
        |(i, j)| vent_grid[GridPosition{x: *i as isize, y: *j as isize}] >= 2
    ).count();

    format!("{}", dangerous_point_count)
}

fn create_grid2(vent_lines: &[VentLine]) -> VentGrid {
    let mut vent_grid = VentGrid::new(GRID_SIZE, GRID_SIZE);
    for vent_line in vent_lines {
        if vent_line.x1 == vent_line.x2 {
            for y in std::cmp::min(vent_line.y1, vent_line.y2)..std::cmp::max(vent_line.y1, vent_line.y2) + 1 {
                vent_grid[GridPosition{x: vent_line.x1 as isize, y: y as isize}] += 1;
            }
        }
        else if vent_line.y1 == vent_line.y2 {
            for x in std::cmp::min(vent_line.x1, vent_line.x2)..std::cmp::max(vent_line.x1, vent_line.x2) + 1 {
                vent_grid[GridPosition{x: x as isize, y: vent_line.y1 as isize}] += 1;
            }
        }
        else {
            assert!((vent_line.x1 - vent_line.x2).abs() == (vent_line.y1 - vent_line.y2).abs());
            for i in 0..(vent_line.x1 - vent_line.x2).abs() + 1 {
                let dx = if vent_line.x2 > vent_line.x1 { i } else { -i };
                let dy = if vent_line.y2 > vent_line.y1 { i } else { -i };
                let x = vent_line.x1 + dx;
                let y = vent_line.y1 + dy;
                vent_grid[GridPosition{x: x as isize, y: y as isize}] += 1;
            }
        }
    }
    vent_grid
}

pub fn part2(input: &str) -> String {
    let vent_lines: Vec<VentLine> = input.lines().map(|line| parse_line(line)).collect();
    let vent_grid = create_grid2(&vent_lines);
    let dangerous_point_count = (0..GRID_SIZE).cartesian_product(0..GRID_SIZE).filter(
        |(i, j)| vent_grid[GridPosition{x: *i as isize, y: *j as isize}] >= 2
    ).count();

    format!("{}", dangerous_point_count)
}