struct Position {
    depth: i32,
    horizontal: i32,
}

impl std::ops::AddAssign for Position {
    fn add_assign(&mut self, rhs: Position) {
        self.depth += rhs.depth;
        self.horizontal += rhs.horizontal;
    }
}

fn parse_line(input: &str) -> (String, i32) {
    let command: String;
    let value: i32;
    text_io::scan!(input.bytes() => "{} {}", command, value);
    (command, value)
}

fn apply_command(command: &str, value: i32) -> std::io::Result<Position> {
    match command {
        "forward" => Ok(Position{ depth: 0, horizontal: value}),
        "down" => Ok(Position{ depth: value, horizontal: 0}),
        "up" => Ok(Position{ depth: -value, horizontal: 0}),
        _ => Err(std::io::Error::from(std::io::ErrorKind::InvalidData)),
    }
}

pub fn part1(input: &str) -> String {
    let mut pos = Position{depth: 0, horizontal: 0};
    input.lines().map(parse_line).for_each(|(command, value)| pos += apply_command(&command, value).unwrap());
    format!("{}", pos.depth * pos.horizontal)
}

fn apply_command_with_aim(command: &str, value: i32, pos: &mut Position, aim: &mut i32) -> std::io::Result<()> {
    match command {
        "forward" => {
            pos.horizontal += value;
            pos.depth += *aim * value;
            Ok(())
        }
        "down" => {
            *aim += value;
            Ok(())
        }
        "up" => {
            *aim -= value;
            Ok(())
        }
        _ => Err(std::io::Error::from(std::io::ErrorKind::InvalidData)),
    }
}

pub fn part2(input: &str) -> String {
    let mut pos = Position{depth: 0, horizontal: 0};
    let mut aim = 0;
    input.lines().map(parse_line).for_each(|(command, value)| apply_command_with_aim(&command, value, &mut pos, &mut aim).unwrap());
    format!("{}", pos.depth * pos.horizontal)
}