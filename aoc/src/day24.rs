use std::collections::HashMap;
use std::rc::Rc;

use lazy_static::lazy_static;

use regex::Regex;

enum Variable
{
    Add(Rc<Variable>, Rc<Variable>),
    Mul(Rc<Variable>, Rc<Variable>),
    Div(Rc<Variable>, Rc<Variable>),
    Mod(Rc<Variable>, Rc<Variable>),
    Eql(Rc<Variable>, Rc<Variable>),
    Value(i32),
    Input(u32),
}

impl std::fmt::Debug for Variable
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        match self
        {
            Self::Add(lhs, rhs) => write!(f, "({:?} + {:?})", *lhs, *rhs),
            Self::Mul(lhs, rhs) => write!(f, "({:?} * {:?})", *lhs, *rhs),
            Self::Div(lhs, rhs) => write!(f, "({:?} / {:?})", *lhs, *rhs),
            Self::Mod(lhs, rhs) => write!(f, "({:?} % {:?})", *lhs, *rhs),
            Self::Eql(lhs, rhs) => write!(f, "({:?} == {:?})", *lhs, *rhs),
            Self::Value(val) => write!(f, "{}", val),
            Self::Input(input_index) => write!(f, "i{}", input_index),
        }
    }
}

fn parse_instruction(line: &str, variables: &mut HashMap<char, Rc<Variable>>, input_counter: &mut u32)
{
    lazy_static! {
        static ref RE: Regex = Regex::new(r"^([a-z]{3}) ([wxyz])(?: ([wxyz]|[\d\-]+))?$").unwrap();
    }

    let captures = RE.captures(line).unwrap();
    let variable_name = captures[2].chars().next().unwrap();

    let rhs = captures.get(3).and_then(|capture| match capture.as_str()
    {
        "x" | "y" | "z" | "w" => Some(variables[&capture.as_str().chars().next().unwrap()].clone()),
        _ => Some(Rc::new(Variable::Value(capture.as_str().parse::<i32>().unwrap()))),
    });

    match &captures[1]
    {
        "inp" => {
            *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Input(*input_counter));
            *input_counter += 1;
        },
        "add" => {
            let rhs_var = rhs.unwrap();
            if let Variable::Value(lhs_val) = *variables[&variable_name]
            {
                if lhs_val == 0
                {
                    *variables.get_mut(&variable_name).unwrap() = rhs_var;
                    return;
                }
            }

            if let Variable::Value(rhs_val) = *rhs_var
            {
                if rhs_val == 0
                {
                    return;
                }

                if let Variable::Value(lhs_val) = *variables[&variable_name]
                {
                    *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Value(lhs_val + rhs_val));
                    return;
                }
            }

            *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Add(variables[&variable_name].clone(), rhs_var));
        },
        "mul" => {
            let rhs_var = rhs.unwrap();
            if let Variable::Value(lhs_val) = *variables[&variable_name]
            {
                if lhs_val == 1
                {
                    *variables.get_mut(&variable_name).unwrap() = rhs_var;
                    return;
                }

                if lhs_val == 0
                {
                    *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Value(0));
                    return;
                }
            }

            if let Variable::Value(rhs_val) = *rhs_var
            {
                if rhs_val == 1
                {
                    return;
                }

                if rhs_val == 0
                {
                    *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Value(0));
                    return;
                }

                if let Variable::Value(lhs_val) = *variables[&variable_name]
                {
                    *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Value(lhs_val * rhs_val));
                    return;
                }
            }

            *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Mul(variables[&variable_name].clone(), rhs_var));
        },
        "div" => {
            let rhs_var = rhs.unwrap();
            if let Variable::Value(lhs_val) = *variables[&variable_name]
            {
                if lhs_val == 0
                {
                    *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Value(0));
                    return;
                }
            }

            if let Variable::Value(rhs_val) = *rhs_var
            {
                if rhs_val == 1
                {
                    return;
                }

                if let Variable::Value(lhs_val) = *variables[&variable_name]
                {
                    *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Value(lhs_val / rhs_val));
                    return;
                }
            }

            *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Div(variables[&variable_name].clone(), rhs_var));
        },
        "mod" => {
            let rhs_var = rhs.unwrap();
            if let Variable::Value(lhs_val) = *variables[&variable_name]
            {
                if lhs_val == 0
                {
                    *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Value(0));
                    return;
                }
            }

            if let Variable::Value(rhs_val) = *rhs_var
            {
                if rhs_val == 1
                {
                    return;
                }

                if let Variable::Value(lhs_val) = *variables[&variable_name]
                {
                    *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Value(lhs_val % rhs_val));
                    return;
                }
            }

            *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Mod(variables[&variable_name].clone(), rhs_var));
        },
        "eql" => {
            let rhs_var = rhs.unwrap();
            if let Variable::Value(rhs_val) = *rhs_var
            {
                if let Variable::Value(lhs_val) = *variables[&variable_name]
                {
                    *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Value(if lhs_val == rhs_val { 1 } else { 0 }));
                    return;
                }
            }

            *variables.get_mut(&variable_name).unwrap() = Rc::new(Variable::Eql(variables[&variable_name].clone(), rhs_var));
        },
        _ => unreachable!()
    }
}

fn parse_instructions(input: &str) -> HashMap<char, Rc<Variable>>
{
    let mut variables = HashMap::with_capacity(4);
    variables.insert('x', Rc::new(Variable::Value(0)));
    variables.insert('y', Rc::new(Variable::Value(0)));
    variables.insert('z', Rc::new(Variable::Value(0)));
    variables.insert('w', Rc::new(Variable::Value(0)));
    let mut input_counter = 0;
    input.lines().for_each(|line| parse_instruction(line, &mut variables, &mut input_counter));
    variables
}

pub fn part1(input: &str) -> String
{
    let variables = parse_instructions(input);
    println!("z = {:?}", variables[&'z']);
    "".to_string()
}

pub fn part2(_input: &str) -> String {
    "".to_string()
}
