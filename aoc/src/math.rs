///////////////////////////////////////// Vector ////////////////////////////////////////////////

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub struct Vector<T: Sized, const DIMENSION: usize>
{
    storage: [T; DIMENSION],
}

impl<T, const DIMENSION: usize> Vector<T, DIMENSION>
{
    pub fn from(values: [T; DIMENSION]) -> Self { Self{ storage: values } }
}

impl<T, const DIMENSION: usize> Default for Vector<T, DIMENSION> where T: Default + Copy
{
    fn default() -> Self { Self{ storage: [T::default(); DIMENSION] } }
}

impl<T> Vector<T, 3>
{
    pub fn new(x: T, y: T, z: T) -> Self { Self{ storage: [x, y, z] } }
}

impl<T, const DIMENSION: usize> std::ops::Index<usize> for Vector<T, DIMENSION>
{
    type Output = T;

    fn index(&self, i: usize) -> &Self::Output
    {
        &self.storage[i]
    }
}

impl<T, const DIMENSION: usize> std::ops::IndexMut<usize> for Vector<T, DIMENSION>
{
    fn index_mut(&mut self, i: usize) -> &mut Self::Output
    {
        &mut self.storage[i]
    }
}

impl<T, const DIMENSION: usize> std::ops::Sub<&Vector<T, DIMENSION>> for &Vector<T, DIMENSION>
    where T: Clone + Copy + std::ops::SubAssign
{
    type Output = Vector<T, DIMENSION>;

    fn sub(self, rhs: &Vector<T, DIMENSION>) -> Self::Output
    {
        let mut result = self.clone();
        for i in 0..DIMENSION
        {
            result[i] -= rhs[i];
        }
        result
    }
}

impl<T, const DIMENSION: usize> std::ops::Neg for &Vector<T, DIMENSION>
    where T: Default + Copy + std::ops::Neg<Output = T>
{
    type Output = Vector<T, DIMENSION>;

    fn neg(self) -> Self::Output
    {
        let mut result = Self::Output::default();
        for i in 0..DIMENSION
        {
            result[i] = -self[i];
        }
        result
    }
}

impl<T, const DIMENSION: usize> std::ops::Add<&Vector<T, DIMENSION>> for &Vector<T, DIMENSION>
    where T: Clone + Copy + std::ops::AddAssign
{
    type Output = Vector<T, DIMENSION>;

    fn add(self, rhs: &Vector<T, DIMENSION>) -> Self::Output
    {
        let mut result = self.clone();
        for i in 0..DIMENSION
        {
            result[i] += rhs[i];
        }
        result
    }
}

impl<T, const DIMENSION: usize> std::fmt::Debug for Vector<T, DIMENSION>
    where T: std::fmt::Debug
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "{:?}", self.storage)
    }
}

pub type Vec3D<T> = Vector<T, 3>;

///////////////////////////////////////// MatrixRow //////////////////////////////////////////////

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub struct MatrixRow<T: Sized, const SIZE: usize>
{
    storage: [T; SIZE],
}

impl<T, const SIZE: usize> MatrixRow<T, SIZE>
{
    pub fn from(values: [T; SIZE]) -> Self { Self{ storage: values } }
}

impl<T, const SIZE: usize> Default for MatrixRow<T, SIZE> where T: Default + Copy
{
    fn default() -> Self { Self{ storage: [T::default(); SIZE] } }
}

impl<T, const SIZE: usize> std::ops::Index<usize> for MatrixRow<T, SIZE>
{
    type Output = T;

    fn index(&self, i: usize) -> &Self::Output
    {
        &self.storage[i]
    }
}

impl<T, const SIZE: usize> std::ops::IndexMut<usize> for MatrixRow<T, SIZE>
{
    fn index_mut(&mut self, i: usize) -> &mut Self::Output
    {
        &mut self.storage[i]
    }
}

impl<T, const SIZE: usize> std::fmt::Debug for MatrixRow<T, SIZE>
    where T: std::fmt::Debug
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "{:?}", self.storage)
    }
}

//////////////////////////////////////////// Matrix //////////////////////////////////////////////

#[derive(Clone, Copy, Eq, Hash, PartialEq)]
pub struct Matrix<T: Sized, const ROWS: usize, const COLUMNS: usize>
{
    storage: [MatrixRow<T, COLUMNS>; ROWS]
}

impl<T, const ROWS: usize, const COLUMNS: usize> Matrix<T, ROWS, COLUMNS> where T: Default + Copy
{
    pub fn from(values: [[T; COLUMNS]; ROWS]) -> Self
    {
        let mut result = Self::default();
        for r in 0..ROWS
        {
            result.storage[r] = MatrixRow::from(values[r]);
        }
        result
    }
}

impl<T, const ROWS: usize, const COLUMNS: usize, const COLUMNS2: usize> std::ops::Mul<&Matrix<T, COLUMNS, COLUMNS2>> for &Matrix<T, ROWS, COLUMNS>
    where T : Default + Copy + std::ops::Mul<Output = T> + std::ops::AddAssign
{
    type Output = Matrix<T, ROWS, COLUMNS2>;

    fn mul(self, rhs: &Matrix<T, COLUMNS, COLUMNS2>) -> Self::Output
    {
        let mut result = Self::Output::default();
        for r in 0..ROWS
        {
            for c2 in 0..COLUMNS2
            {
                for c in 0..COLUMNS
                {
                    result[r][c2] += self[r][c] * rhs[c][c2];
                }
            }
        }
        result
    }
}

impl<T, const ROWS: usize, const COLUMNS: usize> std::ops::Mul<&Vector<T, COLUMNS>> for &Matrix<T, ROWS, COLUMNS>
    where T : Default + Copy + std::ops::Mul<Output = T> + std::ops::AddAssign
{
    type Output = Vector<T, ROWS>;

    fn mul(self, rhs: &Vector<T, COLUMNS>) -> Self::Output
    {
        let mut result = Self::Output::default();
        for r in 0..ROWS
        {
            for c in 0..COLUMNS
            {
                result[r] += self[r][c] * rhs[c];
            }
        }
        result
    }
}

impl<T, const ROWS: usize, const COLUMNS: usize> Matrix<T, ROWS, COLUMNS> where T: Default + Copy
{
    pub fn transpose(&self) -> Self
    {
        let mut transposed = Self::default();
        for i in 0..3
        {
            for j in 0..3
            {
                transposed[i][j] = self[j][i];
            }
        }
        transposed
    }
}

impl<T, const ROWS: usize, const COLUMNS: usize> Default for Matrix<T, ROWS, COLUMNS> where T: Default + Copy
{
    fn default() -> Self { Self{ storage: [MatrixRow::default(); ROWS] } }
}

impl<T, const ROWS: usize, const COLUMNS: usize> std::ops::Index<usize> for Matrix<T, ROWS, COLUMNS>
{
    type Output = MatrixRow<T, COLUMNS>;

    fn index(&self, row: usize) -> &Self::Output
    {
        &self.storage[row]
    }
}

impl<T, const ROWS: usize, const COLUMNS: usize> std::ops::IndexMut<usize> for Matrix<T, ROWS, COLUMNS>
{
    fn index_mut(&mut self, row: usize) -> &mut Self::Output
    {
        &mut self.storage[row]
    }
}

impl<T, const ROWS: usize, const COLUMNS: usize> std::fmt::Debug for Matrix<T, ROWS, COLUMNS>
    where T: std::fmt::Debug
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        write!(f, "{:?}", self.storage)
    }
}

pub type Matrix3D<T> = Matrix<T, 3, 3>;
