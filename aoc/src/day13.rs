use std::collections::HashSet;
use itertools::Itertools;

#[derive(PartialEq, Eq, Hash, Clone, Copy, Debug)]
struct Position {
    x: u32,
    y: u32
}

#[derive(Clone, Copy, Debug)]
enum Fold {
    X(u32),
    Y(u32)
}

fn parse_position(line: &str) -> Position {
    let (x, y) = line.split(',').map(|pos| pos.parse::<u32>().unwrap()).collect_tuple().unwrap();
    Position{ x: x, y: y }
}

fn parse_fold(line: &str) -> Fold {
    let axis: char;
    let value: u32;
    text_io::scan!(line.bytes() => "fold along {}={}", axis, value);
    if axis == 'x' { Fold::X(value) } else { assert!(axis == 'y'); Fold::Y(value) }
}
fn parse_input(input: &str) -> (HashSet<Position>, Vec<Fold>) {

    let mut line_iter = input.lines();
    let positions = line_iter.by_ref()
        .take_while(|line| !line.is_empty())
        .map(parse_position)
        .collect();

    let folds = line_iter.by_ref()
        .map(parse_fold)
        .collect();

    (positions, folds)
}

fn fold_x(value: u32, pos: &Position) -> Option<Position> {
    if pos.x > value { Some(Position{x: 2 * value - pos.x, y: pos.y}) } else { None }
}

fn fold_y(value: u32, pos: &Position) -> Option<Position> {
    if pos.y > value { Some(Position{x: pos.x, y: 2 * value - pos.y}) } else { None }
}

fn fold(fold_command: Fold, positions: &HashSet<Position>) -> HashSet<Position>{
    let (fold_fn, fold_val): (fn(u32, &Position) -> Option<Position>, u32) = match fold_command {
        Fold::X(val) => (fold_x, val),
        Fold::Y(val) => (fold_y, val),
    };

    let mut new_positions = HashSet::new();
    for pos in positions.iter() {
        if let Some(new_pos) = fold_fn(fold_val, &pos) {
            new_positions.insert(new_pos);
        }
        else {
            new_positions.insert(*pos);
        }
    }
    new_positions
}

pub fn part1(input: &str) -> String {
    let (mut positions, folds) = parse_input(&input);
    positions = fold(folds[0], &positions);
    format!("{}", positions.len())
}

pub fn part2(input: &str) -> String {
    let (mut positions, folds) = parse_input(&input);
    for fold_command in &folds {
        positions = fold(*fold_command, &positions);
    }
    let mut max_pos = Position{x: 0, y: 0};
    for pos in &positions {
        if pos.x > max_pos.x {
            max_pos.x = pos.x;
        }
        if pos.y > max_pos.y {
            max_pos.y = pos.y;
        }
    }
    let mut folded_paper = vec![vec!['.'; (max_pos.x+1) as usize]; (max_pos.y+1) as usize];
    for pos in &positions {
        folded_paper[pos.y as usize][pos.x as usize] = '#';
    }

    folded_paper.iter().map(|x| format!("{:?}\n", x)).join("\n")
}
