use itertools::Itertools;

const BOARD_SIZE: usize = 5;

type Board = [[i32; BOARD_SIZE]; BOARD_SIZE];
type MarkedBoard = [[bool; BOARD_SIZE]; BOARD_SIZE];

fn parse_boards(mut line_iter: std::str::Lines) -> Vec<Board> {
    let mut boards = Vec::<Board>::new();

    while let Some(line) = line_iter.next() {
        assert!(line.is_empty());
        let mut board = Board::default();
        for i in 0..BOARD_SIZE {
            line_iter.next().unwrap().split_whitespace().enumerate().for_each(|(j, number)|
                board[i][j] = number.parse::<i32>().unwrap()
            );
        }
        boards.push(board);
    }
    boards
}

fn mark_board(number: i32, board: &Board, marked_board: &mut MarkedBoard) {
    for i in 0..BOARD_SIZE {
        for j in 0..BOARD_SIZE {
            if board[i][j] == number {
                assert!(!marked_board[i][j]);
                marked_board[i][j] = true;
            }
        }
    }
}

fn check_winning(marked_board: &MarkedBoard) -> bool {
    (0..BOARD_SIZE).any(|i|
        (0..BOARD_SIZE).all(|j| marked_board[i][j]) || (0..BOARD_SIZE).all(|j| marked_board[j][i])
    )
}

fn compute_score(number: i32, board: &Board, marked_board: &MarkedBoard) -> i32 {
    let unmarked_sum: i32 = (0..BOARD_SIZE).cartesian_product(0..BOARD_SIZE).map(
        |(i, j)| if marked_board[i][j] { 0 } else { board[i][j] }
    ).sum();
    number * unmarked_sum
}

pub fn part1(input: &str) -> String {
    let mut line_iter = input.lines();
    let numbers: Vec<i32> = line_iter.next().unwrap().split(',').map(|x| x.parse::<i32>().unwrap()).collect();
    let boards = parse_boards(line_iter);
    let mut marked_boards= vec![[[false; BOARD_SIZE]; BOARD_SIZE]; boards.len()];

    let mut number_index = 0;
    let mut winning_board_index: i32 = -1;
    while winning_board_index < 0 {
        assert!(number_index < numbers.len());
        let number = numbers[number_index];

        for i in 0..boards.len() {
            mark_board(number, &boards[i], &mut marked_boards[i]);

            let is_winning_board = check_winning(&marked_boards[i]);
            if is_winning_board {
                winning_board_index = i as i32;
                break;
            }
        }

        if winning_board_index < 0 {
            number_index += 1;
        }
    }

    let winning_number = numbers[number_index];
    let winning_board = &boards[winning_board_index as usize];
    let winning_marked_board = &marked_boards[winning_board_index as usize];
    let score = compute_score(winning_number, winning_board , winning_marked_board);
    format!("{}", score)
}

pub fn part2(input: &str) -> String {
    let mut line_iter = input.lines();
    let numbers: Vec<i32> = line_iter.next().unwrap().split(',').map(|x| x.parse::<i32>().unwrap()).collect();
    let boards = parse_boards(line_iter);
    let mut marked_boards= vec![[[false; BOARD_SIZE]; BOARD_SIZE]; boards.len()];
    let mut won_boards = vec![false; boards.len()];

    let mut last_winning_board_index: i32 = -1;
    let mut last_winning_number: i32 = -1;
    for number in &numbers {
        for i in 0..boards.len() {
            if !won_boards[i] {
                mark_board(*number, &boards[i], &mut marked_boards[i]);

                let is_winning_board = check_winning(&marked_boards[i]);
                if is_winning_board {
                    won_boards[i] = true;
                    last_winning_board_index = i as i32;
                    last_winning_number = *number;
                }
            }
        }
    }

    let winning_board = &boards[last_winning_board_index as usize];
    let winning_marked_board = &marked_boards[last_winning_board_index as usize];
    let score = compute_score(last_winning_number, winning_board , winning_marked_board);
    format!("{}", score)
}