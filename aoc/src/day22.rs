use std::cmp::{min, max};

#[derive(Copy, Clone, Debug)]
struct Region3D
{
    x: (i32, i32),
    y: (i32, i32),
    z: (i32, i32),
}

impl Region3D
{
    fn overlaps(&self, other: &Region3D) -> bool
    {
        if other.x.0 > self.x.1 || other.x.1 < self.x.0 ||
            other.y.0 > self.y.1 || other.y.1 < self.y.0 ||
            other.z.0 > self.z.1 || other.z.1 < self.z.0
        {
            false
        }
        else
        {
            true
        }
    }

    fn volume(&self) -> u64
    {
        (self.x.1 + 1 - self.x.0) as u64 * (self.y.1 + 1 - self.y.0) as u64 * (self.z.1 + 1 - self.z.0) as u64
    }
}

#[derive(Copy, Clone, Debug)]
struct RebootStep
{
    region: Region3D,
    state: u8,
}

fn parse_reboot_step(line: &str, min_val: i32, max_val: i32) -> Option<RebootStep>
{
    let x_start: i32;
    let x_end: i32;
    let y_start: i32;
    let y_end: i32;
    let z_start: i32;
    let z_end: i32;
    let state: String;
    text_io::scan!(line.bytes() => "{} x={}..{},y={}..{},z={}..{}", state, x_start, x_end, y_start, y_end, z_start, z_end);
    let region = Region3D
    {
        x: (max(x_start, min_val), min(x_end, max_val)),
        y: (max(y_start, min_val), min(y_end, max_val)),
        z: (max(z_start, min_val), min(z_end, max_val))
    };

    if region.x.0 > max_val || region.x.1 < min_val ||
        region.y.0 > max_val || region.y.1 < min_val ||
        region.z.0 > max_val || region.z.1 < min_val
    {
        None
    }
    else
    {
        Some(RebootStep{ region, state: if state == "on" { 1 } else { 0 }})
    }
}

fn split_region(region: &Region3D, i: usize, on_regions: &mut Vec<Region3D>) -> bool
{
    if !region.overlaps(&on_regions[i])
    {
        return false;
    }

    if region.x.0 > on_regions[i].x.0
    {
        {
            let mut new_region = on_regions[i];
            new_region.x.0 = region.x.0;
            on_regions.push(new_region);
            split_region(region, on_regions.len() - 1, on_regions);
        }

        {
            let mut new_region = on_regions[i];
            new_region.x.1 = region.x.0 - 1;
            on_regions.push(new_region);
            split_region(region, on_regions.len() - 1, on_regions);
        }
    }
    else if region.x.1 < on_regions[i].x.1
    {
        {
            let mut new_region = on_regions[i];
            new_region.x.0 = region.x.1 + 1;
            on_regions.push(new_region);
            split_region(region, on_regions.len() - 1, on_regions);
        }

        {
            let mut new_region = on_regions[i];
            new_region.x.1 = region.x.1;
            on_regions.push(new_region);
            split_region(region, on_regions.len() - 1, on_regions);
        }

    }
    else if region.y.0 > on_regions[i].y.0
    {
        {
            let mut new_region = on_regions[i];
            new_region.y.0 = region.y.0;
            on_regions.push(new_region);
            split_region(region, on_regions.len() - 1, on_regions);
        }

        {
            let mut new_region = on_regions[i];
            new_region.y.1 = region.y.0 - 1;
            on_regions.push(new_region);
            split_region(region, on_regions.len() - 1, on_regions);
        }
    }
    else if region.y.1 < on_regions[i].y.1
    {
        {
            let mut new_region = on_regions[i];
            new_region.y.0 = region.y.1 + 1;
            on_regions.push(new_region);
            split_region(region, on_regions.len() - 1, on_regions);
        }

        {
            let mut new_region = on_regions[i];
            new_region.y.1 = region.y.1;
            on_regions.push(new_region);
            split_region(region, on_regions.len() - 1, on_regions);
        }

    }
    else if region.z.0 > on_regions[i].z.0
    {
        {
            let mut new_region = on_regions[i];
            new_region.z.0 = region.z.0;
            on_regions.push(new_region);
            split_region(region, on_regions.len() - 1, on_regions);
        }

        {
            let mut new_region = on_regions[i];
            new_region.z.1 = region.z.0 - 1;
            on_regions.push(new_region);
            split_region(region, on_regions.len() - 1, on_regions);
        }
    }
    else if region.z.1 < on_regions[i].z.1
    {
        {
            let mut new_region = on_regions[i];
            new_region.z.0 = region.z.1 + 1;
            on_regions.push(new_region);
            split_region(region, on_regions.len() - 1, on_regions);
        }

        {
            let mut new_region = on_regions[i];
            new_region.z.1 = region.z.1;
            on_regions.push(new_region);
            split_region(region, on_regions.len() - 1, on_regions);
        }

    }

    on_regions.remove(i);

    return true;
}

fn process_reboot_step(reboot_step : &RebootStep, on_regions: &mut Vec<Region3D>)
{
    let mut regions_to_test = on_regions.len();
    let mut i = 0;
    while i < regions_to_test
    {
        if split_region(&reboot_step.region, i, on_regions)
        {
            regions_to_test -= 1;
        }
        else
        {
            i += 1;
        }
    }

    if reboot_step.state == 1
    {
        on_regions.push(reboot_step.region);
    }
}

pub fn part1(input: &str) -> String {
    let reboot_steps = input.lines().filter_map(|line| parse_reboot_step(line, -50, 50)).collect::<Vec<_>>();
    // println!("{:?}", reboot_steps);
    let mut on_regions = Vec::<Region3D>::new();
    reboot_steps.iter().for_each(|reboot_step| process_reboot_step(reboot_step, &mut on_regions));
    // println!("{:?}", on_regions);
    let cube_on_count = on_regions.iter().map(|region| region.volume()).sum::<u64>();
    format!("{}", cube_on_count)
}

pub fn part2(input: &str) -> String {
    let reboot_steps = input.lines().filter_map(|line| parse_reboot_step(line, i32::MIN, i32::MAX)).collect::<Vec<_>>();
    // println!("{:?}", reboot_steps);
    let mut on_regions = Vec::<Region3D>::new();
    reboot_steps.iter().for_each(|reboot_step| process_reboot_step(reboot_step, &mut on_regions));
    // println!("{:?}", on_regions);
    let cube_on_count = on_regions.iter().map(|region| region.volume()).sum::<u64>();
    format!("{}", cube_on_count)
}
