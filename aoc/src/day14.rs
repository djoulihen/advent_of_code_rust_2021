use std::collections::HashMap;
use itertools::Itertools;

fn parse_insertion_rule(line: &str) -> (&str, char) {
    let (pair, element) = line.split(" -> ").collect_tuple().unwrap();
    (pair, element.chars().next().unwrap())
}

fn parse_input(input: &str) -> (&str, HashMap<&str, char>) {
    let mut line_iter = input.lines();

    let polymer_template = line_iter.next().unwrap();
    assert!(line_iter.next().unwrap().is_empty());

    let insertion_rules: HashMap<&str, char> = line_iter
        .map(parse_insertion_rule)
        .collect();
    (polymer_template, insertion_rules)
}

fn polymerize(polymer: &str, insertion_rules: &HashMap<&str, char>) -> String {
    let mut result = String::with_capacity(polymer.len() * 2 - 1);
    for i in 0..polymer.len()-1 {
        let element_pair = &polymer[i..i+2];
        assert!(insertion_rules.contains_key(element_pair));
        let mut char_iter = element_pair.chars();
        result.push(char_iter.next().unwrap());
        result.push(insertion_rules[element_pair]);
    }
    result.push(polymer.chars().last().unwrap());
    result
}

fn string_histogram(input: &str) -> HashMap<char, u64> {
    let mut histogram = HashMap::new();
    input.chars().for_each(|c| *histogram.entry(c).or_insert(0) += 1);
    histogram
}

fn compute_score_from_histogram(histogram: &HashMap<char, u64>) -> u64 {
    let minmax = histogram.values().minmax();
    if let itertools::MinMaxResult::MinMax(min, max) = minmax { max - min } else { unreachable!() }
}

fn compute_score1(polymer: &str) -> u64 {
    let histogram = string_histogram(polymer);
    compute_score_from_histogram(&histogram)
}

pub fn part1(input: &str) -> String {
    const NB_STEPS: u32 = 10;
    let (polymer_template, insertion_rules) = parse_input(&input);
    let mut polymer: String = polymer_template.to_string();
    for _ in 0..NB_STEPS {
        polymer = polymerize(&polymer, &insertion_rules);
    }
    let score = compute_score1(&polymer);
    format!("{}", score)
}

fn merge_histograms(mut histogram1: HashMap<char, u64>, histogram2: HashMap<char, u64>) -> HashMap<char, u64> {
    histogram2.iter().for_each(|(c, count)| *histogram1.entry(*c).or_insert(0) += count);
    histogram1
}

fn update_histogram(
    element_pair: String,
    insertion_rules: &HashMap<&str, char>,
    cache: &mut HashMap<(String, u32), HashMap<char, u64>>,
    depth_to_go: u32
) -> HashMap<char, u64> {
    assert!(depth_to_go > 0);

    let cache_key = (element_pair, depth_to_go);
    if let Some(histogram) = cache.get(&cache_key) {
        return histogram.clone();
    }

    assert!(insertion_rules.contains_key(cache_key.0.as_str()));
    let inserted_char = insertion_rules[cache_key.0.as_str()];

    let mut pair_iter = cache_key.0.chars();
    let mut histogram: HashMap<char, u64>;
    if depth_to_go > 1
    {
        let mut left_pair = String::with_capacity(2);
        left_pair.push(pair_iter.next().unwrap());
        left_pair.push(inserted_char);
        histogram = update_histogram(left_pair, insertion_rules, cache, depth_to_go-1);

        let mut right_pair = String::with_capacity(2);
        right_pair.push(inserted_char);
        right_pair.push(pair_iter.next().unwrap());
        let right_histogram = update_histogram(right_pair, insertion_rules, cache, depth_to_go-1);
        histogram = merge_histograms(histogram, right_histogram);
    }
    else {
        histogram = HashMap::<char, u64>::new();
    }

    *histogram.entry(inserted_char).or_insert(0) += 1;

    cache.insert((cache_key.0, depth_to_go), histogram.clone());
    histogram
}

pub fn part2(input: &str) -> String {
    const NB_STEPS: u32 = 40;
    let (polymer_template, insertion_rules) = parse_input(&input);
    let mut histogram = HashMap::new();
    let mut cache = HashMap::new();
    for i in 0..polymer_template.len()-1 {
        let element_pair = &polymer_template[i..i+2];
        let sub_histogram = update_histogram(element_pair.to_string(), &insertion_rules, &mut cache, NB_STEPS);
        histogram = merge_histograms(histogram, sub_histogram);
    }

    for c in polymer_template.chars() {
        *histogram.entry(c).or_insert(0) += 1;
    }

    let score = compute_score_from_histogram(&histogram);
    format!("{}", score)
}
