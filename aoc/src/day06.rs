fn reproduce(fishes: &mut Vec<u8>) {
    for i in 0..fishes.len() {
        if fishes[i] == 0 {
            fishes.push(8);
            fishes[i] = 6;
        }
        else {
            fishes[i] -= 1;
        }
    }
}

pub fn part1(input: &str) -> String {
    const NUM_DAYS: i32 = 80;
    let mut fishes: Vec<u8> = input.trim().split(',').map(|x| x.parse::<u8>().unwrap()).collect();
    for _ in 0..NUM_DAYS {
        reproduce(&mut fishes);
    }
    format!("{}", fishes.len())
}

fn reproduce2(fishes: &mut Vec<i64>) {
    let new_fishes = fishes[0];
    for i in 0..8 {
        fishes[i] = fishes[i+1];
    }
    fishes[8] = new_fishes;
    fishes[6] += new_fishes;
}

pub fn part2(input: &str) -> String {
    const NUM_DAYS: i32 = 256;
    let mut fishes: Vec<i64> = vec![0; 9];
    input.trim().split(',').for_each(|x| fishes[x.parse::<usize>().unwrap()] += 1);
    for _ in 0..NUM_DAYS {
        reproduce2(&mut fishes);
    }
    format!("{}", fishes.iter().sum::<i64>())
}