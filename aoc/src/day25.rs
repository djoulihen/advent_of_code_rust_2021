use crate::grid::{Grid, GridPosition, HeapGrid};

fn parse_seafloor(input: &str) -> HeapGrid<u8>
{
    let grid_width = input.lines().next().unwrap().len();
    let grid_height = input.lines().filter(|l| !l.is_empty()).count();
    let mut grid = HeapGrid::<u8>::new(grid_width, grid_height);

    for (l, line) in input.lines().enumerate()
    {
        if line.is_empty()
        {
            continue;
        }

        for (i, c) in line.chars().enumerate()
        {
            grid[GridPosition{x: i as isize, y: l as isize}] = match c {
                '.' => 0,
                '>' => 1,
                'v' => 2,
                _ => unreachable!(),
            };
        }
    }
    grid
}

fn move_cucumbers_east(seafloor: &mut HeapGrid<u8>, moved: &mut HeapGrid<bool>) -> u32
{
    let mut move_count = 0;
    moved.values_mut().for_each(|val| *val = false);
    for pos in seafloor.positions()
    {
        if seafloor[pos] != 1 || moved[pos]
        {
            continue;
        }

        let mut neighbor_pos = pos;
        neighbor_pos.x = (neighbor_pos.x + 1) % seafloor.width() as isize;
        if seafloor[neighbor_pos] == 0 && !moved[neighbor_pos]
        {
            seafloor[neighbor_pos] = 1;
            seafloor[pos] = 0;
            moved[pos] = true;
            moved[neighbor_pos] = true;
            move_count += 1;
        }
    }
    move_count
}

fn move_cucumbers_south(seafloor: &mut HeapGrid<u8>, moved: &mut HeapGrid<bool>) -> u32
{
    let mut move_count = 0;
    moved.values_mut().for_each(|val| *val = false);
    for pos in seafloor.positions()
    {
        if seafloor[pos] != 2 || moved[pos]
        {
            continue;
        }

        let mut neighbor_pos = pos;
        neighbor_pos.y = (neighbor_pos.y + 1) % seafloor.height() as isize;
        if seafloor[neighbor_pos] == 0 && !moved[neighbor_pos]
        {
            seafloor[neighbor_pos] = 2;
            seafloor[pos] = 0;
            moved[pos] = true;
            moved[neighbor_pos] = true;
            move_count += 1;
        }
    }
    move_count
}

fn move_cucumbers(seafloor: &mut HeapGrid<u8>, moved: &mut HeapGrid<bool>) -> u32
{
    move_cucumbers_east(seafloor, moved) + move_cucumbers_south(seafloor, moved)
}

pub fn part1(input: &str) -> String
{
    let mut seafloor = parse_seafloor(input);
    let mut moved = HeapGrid::new(seafloor.width(), seafloor.height());
    let mut step = 1;
    while move_cucumbers(&mut seafloor, &mut moved) > 0
    {
        step += 1;
    }
    format!("{}", step)
}

pub fn part2(_input: &str) -> String {
    "".to_string()
}
