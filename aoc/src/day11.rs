const GRID_SIZE: usize = 10;

type OctopusGrid = [[u8; GRID_SIZE]; GRID_SIZE];

fn parse_grid(input: &str) -> OctopusGrid {
    let mut grid: OctopusGrid = [[0; GRID_SIZE]; GRID_SIZE];
    input.lines().enumerate().for_each(|(i, line)|
        line.chars().enumerate().for_each(|(j, c)| grid[i][j] = c.to_digit(10).unwrap() as u8)
    );
    grid
}

fn increase_octopus_energy(i: usize, j: usize, grid: &mut OctopusGrid) {
    let energy = &mut grid[i][j];
    if *energy >= 10 {
        assert_eq!(*energy, 10);
        return;
    }

    *energy += 1;
    if *energy != 10 {
        return;
    }

    for di in [-1, 0, 1] {
        let n_i = i as i32 + di;
        for dj in [-1, 0, 1] {
            let n_j = j as i32 + dj;
            if (di != 0 || dj != 0) && n_i >= 0 && n_i < GRID_SIZE as i32 && n_j >= 0 && n_j < GRID_SIZE as i32 {
                increase_octopus_energy(n_i as usize, n_j as usize, grid)
            }
        }
    }
}

fn increase_grid_energy(grid: &mut OctopusGrid) -> u32 {
    for i in 0..GRID_SIZE {
        for j in 0..GRID_SIZE {
            increase_octopus_energy(i, j, grid);
        }
    }

    let mut nb_flashes = 0_u32;
    for element in grid.iter_mut().flat_map(|r| r.iter_mut()) {
        if *element == 10 {
            nb_flashes += 1;
            *element = 0;
        }
    }
    nb_flashes
}

pub fn part1(input: &str) -> String {
    const NB_STEPS: u32 = 100;
    let mut grid = parse_grid(&input);

    let mut nb_flashes = 0_u32;
    for _ in 0..NB_STEPS {
        nb_flashes += increase_grid_energy(&mut grid);
    }
    format!("{}", nb_flashes)
}

pub fn part2(input: &str) -> String {
    let mut grid = parse_grid(&input);

    let mut synchronized_flash_step = -1;
    let mut step = 1;
    while synchronized_flash_step == -1 {
        if increase_grid_energy(&mut grid) == (GRID_SIZE * GRID_SIZE) as u32{
            synchronized_flash_step = step;
        }
        else {
            step += 1;
        }
    }
    format!("{}", synchronized_flash_step)
}
