use crate::grid::{Grid, GridPosition, HeapGrid};

use itertools::Itertools;

use std::str::Lines;

fn parse_char(c: char) -> u8
{
    match c {'.' => 0_u8, '#' => 1_u8, _ => unreachable!() }
}

fn parse_enhance(line: &str) -> Vec<u8>
{
    line.chars().map(parse_char).collect()
}

fn parse_image(lines: &mut Lines) -> HeapGrid<u8>
{
    let line = lines.next().unwrap();
    let grid_size = line.len();
    let mut grid = HeapGrid::<u8>::new(grid_size, grid_size);
    for (i, c) in line.chars().enumerate()
    {
        grid[GridPosition{x: i as isize, y: 0}] = parse_char(c);
    }

    for (l, line) in lines.enumerate()
    {
        for (i, c) in line.chars().enumerate()
        {
            grid[GridPosition{x: i as isize, y: l as isize + 1}] = parse_char(c);
        }
    }
    grid
}

fn enhance(image: &HeapGrid<u8>, enhance_algorithm: &Vec<u8>, step: u32) -> HeapGrid<u8>
{
    let mut enhanced_image = HeapGrid::<u8>::new(image.width() + 2, image.height() + 2);

    for pos in enhanced_image.positions()
    {
        let pos_in_image = GridPosition{x: pos.x - 1, y: pos.y - 1};
        let pixel_code = pos_in_image
            .kernel(3, 3)
            .map(|n_pos| if image.is_inside(n_pos) { image[n_pos] } else { (step % 2) as u8 } )
            .fold(0_u32, | folded, v| (folded << 1) | v as u32 );
        enhanced_image[pos] = enhance_algorithm[pixel_code as usize];
    }
    enhanced_image
}

#[allow(dead_code)]
fn display(image: &HeapGrid<u8>)
{
    let display_string = (0..image.height())
        .map(|y|
            (0..image.width())
                .map(|x| match image[GridPosition{x: x as isize, y: y as isize}] { 0 => '.', 1 => '#', _ => unreachable!() } )
                .join(""))
        .join("\n");
    println!("{}\n", display_string);
}

fn solution(input: &str, steps: u32) -> usize
{
    let mut lines = input.lines();
    let enhance_algorithm = parse_enhance(lines.next().unwrap());
    assert!(lines.next().unwrap().is_empty());
    let image = parse_image(&mut lines);
    // display(&image);

    let mut current_image = image;
    for step in 0..steps
    {
        current_image = enhance(&current_image, &enhance_algorithm, step);
        // display(&current_image);
    }

    current_image.values().filter(|v| **v == 1).count()
}

pub fn part1(input: &str) -> String
{
    const STEPS: u32 = 2;
    let pixel_count = solution(input, STEPS);
    format!("{}", pixel_count)
}

pub fn part2(input: &str) -> String
{
    const STEPS: u32 = 50;
    let pixel_count = solution(input, STEPS);
    format!("{}", pixel_count)
}
