use crate::bit_stream::{BitStream, BitIterator};

#[derive(Debug)]
enum OperatorType
{
    Sum,
    Product,
    Min,
    Max,
    GreaterThan,
    LessThan,
    EqualTo,
}

#[derive(Debug)]
enum PacketData
{
    Literal(u64),
    Operator(OperatorType, Vec<Packet>),
}

#[derive(Debug)]
struct Packet
{
    version: u8,
    data: PacketData,
}

fn parse_literal(bit_iter: &mut BitIterator) -> u64
{
    let mut result: u64 = 0;
    loop
    {
        let literal_chunk = bit_iter.concat_next(5).unwrap() as u64;
        result <<= 4;
        result |= literal_chunk & 0xF;

        if literal_chunk & (1 << 4) == 0
        {
            break;
        }
    }
    result
}

fn parse_operator_sub_packets(bit_iter: &mut BitIterator) -> Vec<Packet>
{
    let length_type_id = bit_iter.next().unwrap();
    let mut sub_packets;
    if length_type_id == 0
    {
        let mut length_in_bits = bit_iter.concat_next(15).unwrap() as usize;
        sub_packets = Vec::<Packet>::new();
        while length_in_bits > 0
        {
            let index_before = bit_iter.index();
            sub_packets.push(parse_packet(bit_iter));
            let packet_size = bit_iter.index() - index_before;

            assert!(packet_size <= length_in_bits);
            length_in_bits -= packet_size;
        }
    }
    else
    {
        assert_eq!(length_type_id, 1);
        let sub_packet_count = bit_iter.concat_next(11).unwrap() as usize;
        sub_packets = Vec::<Packet>::with_capacity(sub_packet_count);
        for _ in 0..sub_packet_count
        {
            sub_packets.push(parse_packet(bit_iter));
        }
    }

    sub_packets
}

fn parse_packet(bit_iter: &mut BitIterator) -> Packet
{
    let version = bit_iter.concat_next(3).unwrap() as u8;
    let type_id = bit_iter.concat_next(3).unwrap() as u8;
    let data = match type_id
    {
        0 => PacketData::Operator(OperatorType::Sum, parse_operator_sub_packets(bit_iter)),
        1 => PacketData::Operator(OperatorType::Product, parse_operator_sub_packets(bit_iter)),
        2 => PacketData::Operator(OperatorType::Min, parse_operator_sub_packets(bit_iter)),
        3 => PacketData::Operator(OperatorType::Max, parse_operator_sub_packets(bit_iter)),
        4 => PacketData::Literal(parse_literal(bit_iter)),
        5 => PacketData::Operator(OperatorType::GreaterThan, parse_operator_sub_packets(bit_iter)),
        6 => PacketData::Operator(OperatorType::LessThan, parse_operator_sub_packets(bit_iter)),
        7 => PacketData::Operator(OperatorType::EqualTo, parse_operator_sub_packets(bit_iter)),
        _ => unreachable!(),
    };
    Packet{ version, data }
}

fn sum_versions(packet: &Packet) -> u32
{
    let mut version_sum = 0;
    version_sum += packet.version as u32;
    if let PacketData::Operator(_, sub_packets) = &packet.data {
        version_sum += sub_packets.iter().map(sum_versions).sum::<u32>();
    }
    version_sum
}

pub fn part1(input: &str) -> String
{
    let bit_stream = BitStream::from_hex_string(&input.lines().next().unwrap());
    // format!("{:?}", bit_stream)
    let mut bit_iter = bit_stream.iter();
    let packet = parse_packet(&mut bit_iter);
    // format!("{:#?}", packet)
    assert!(bit_iter.all(|b| b == 0));

    let version_sum = sum_versions(&packet);
    format!("{}", version_sum)
}

fn evaluate(packet: &Packet) -> u64
{
    match &packet.data
    {
        PacketData::Literal(val) => *val,
        PacketData::Operator(OperatorType::Sum, packets) => packets.iter().map(evaluate).sum::<u64>(),
        PacketData::Operator(OperatorType::Product, packets) => packets.iter().map(evaluate).product::<u64>(),
        PacketData::Operator(OperatorType::Min, packets) => packets.iter().map(evaluate).min().unwrap(),
        PacketData::Operator(OperatorType::Max, packets) => packets.iter().map(evaluate).max().unwrap(),
        PacketData::Operator(OperatorType::GreaterThan, packets) =>
        {
            assert_eq!(packets.len(), 2);
            if evaluate(&packets[0]) > evaluate(&packets[1]) { 1 } else { 0 }
        }
        PacketData::Operator(OperatorType::LessThan, packets) =>
        {
            assert_eq!(packets.len(), 2);
            if evaluate(&packets[0]) < evaluate(&packets[1]) { 1 } else { 0 }
        }
        PacketData::Operator(OperatorType::EqualTo, packets) =>
        {
            assert_eq!(packets.len(), 2);
            if evaluate(&packets[0]) == evaluate(&packets[1]) { 1 } else { 0 }
        }
    }
}

pub fn part2(input: &str) -> String {
    let bit_stream = BitStream::from_hex_string(&input.lines().next().unwrap());
    // format!("{:?}", bit_stream)
    let mut bit_iter = bit_stream.iter();
    let packet = parse_packet(&mut bit_iter);
    // format!("{:#?}", packet)
    assert!(bit_iter.all(|b| b == 0));

    let result = evaluate(&packet);
    format!("{}", result)
}
