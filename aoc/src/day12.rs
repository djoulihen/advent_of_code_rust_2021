use itertools::Itertools;
use std::collections::HashMap;

struct Cave {
    visited: u32,
    is_big: bool,
    linked_caves: Vec<usize>
}

struct CaveSystem {
    caves: Vec<Cave>,
    start: usize,
    end: usize,
    visited_twice: bool
}

fn get_or_create_cave<'a>(cave_name: &'a str, cave_system: &mut CaveSystem, cave_name_to_index: &mut HashMap<&'a str, usize>) -> usize {

    if cave_name_to_index.contains_key(cave_name) {
        return cave_name_to_index[cave_name];
    }

    let is_big = cave_name.chars().all(|c| c.is_uppercase());
    let cave = Cave{ visited: 0, is_big: is_big, linked_caves: Vec::<usize>::new() };
    let cave_index = cave_system.caves.len();
    cave_name_to_index.insert(cave_name, cave_index);
    cave_system.caves.push(cave);

    if cave_name == "start" {
        cave_system.start = cave_index;
    }
    else if cave_name == "end" {
        cave_system.end = cave_index;
    }

    cave_index
}

fn parse_cave_system(input: &str) -> CaveSystem {
    let mut cave_system = CaveSystem{ caves: Vec::<Cave>::new(), start: 0, end: 0, visited_twice: false };
    let mut cave_name_to_index = HashMap::new();
    for line in input.lines() {
        let (from, to) = line.split("-").collect_tuple().unwrap();

        let from_index = get_or_create_cave(from, &mut cave_system, &mut cave_name_to_index);
        let to_index = get_or_create_cave(to, &mut cave_system, &mut cave_name_to_index);

        let from_cave = &mut cave_system.caves[from_index];
        from_cave.linked_caves.push(to_index);

        let to_cave = &mut cave_system.caves[to_index];
        to_cave.linked_caves.push(from_index);
    }

    cave_system
}

fn compute_path_count(start: usize, cave_system: &mut CaveSystem) -> u32 {
    if start == cave_system.end
    {
        return 1;
    }

    cave_system.caves[start].visited += 1;

    let mut path_count = 0_u32;
    for i in 0..cave_system.caves[start].linked_caves.len() {
        let neighbor_cave_index = cave_system.caves[start].linked_caves[i];
        if cave_system.caves[neighbor_cave_index].is_big || cave_system.caves[neighbor_cave_index].visited == 0 {
            path_count += compute_path_count(neighbor_cave_index, cave_system);
        }
    }

    cave_system.caves[start].visited -= 1;

    path_count
}

pub fn part1(input: &str) -> String {
    let mut cave_system = parse_cave_system(&input);
    let path_count = compute_path_count(cave_system.start, &mut cave_system);
    format!("{}", path_count)
}

fn compute_path_count2(start: usize, cave_system: &mut CaveSystem) -> u32 {
    if start == cave_system.end
    {
        return 1;
    }

    cave_system.caves[start].visited += 1;
    if !cave_system.caves[start].is_big && cave_system.caves[start].visited > 1 {
        assert!(!cave_system.visited_twice);
        cave_system.visited_twice = true;
    }

    let mut path_count = 0_u32;
    for i in 0..cave_system.caves[start].linked_caves.len() {
        let neighbor_cave_index = cave_system.caves[start].linked_caves[i];
        let allowed_visits = if cave_system.visited_twice { 1 } else { 2 };
        if neighbor_cave_index != cave_system.start &&
            (cave_system.caves[neighbor_cave_index].is_big || cave_system.caves[neighbor_cave_index].visited < allowed_visits){
            path_count += compute_path_count2(neighbor_cave_index, cave_system);
        }
    }

    cave_system.caves[start].visited -= 1;
    if !cave_system.caves[start].is_big && cave_system.caves[start].visited == 1 {
        assert!(cave_system.visited_twice);
        cave_system.visited_twice = false;
    }

    path_count
}

pub fn part2(input: &str) -> String {
    let mut cave_system = parse_cave_system(&input);
    let path_count = compute_path_count2(cave_system.start, &mut cave_system);
    format!("{}", path_count)
}
