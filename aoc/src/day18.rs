use std::cell::RefCell;
use std::fmt::Debug;
use std::rc::Rc;

#[derive(Clone)]
enum Number
{
    Pair(Rc<RefCell<Number>>, Rc<RefCell<Number>>),
    Value(u8)
}

impl Number
{
    fn magnitude(&self) -> u64
    {
        match self
        {
            Self::Value(val) => *val as u64,
            Self::Pair(left, right) => 3 * left.borrow().magnitude() + 2 * right.borrow().magnitude(),
        }
    }
}

impl Debug for Number
{
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result
    {
        match &self
        {
            Number::Value(val) => write!(f, "{}", *val),
            Number::Pair(left, right) =>
                write!(f, "[{:?},{:?}]", left.borrow(), right.borrow()),
        }
    }
}

type NumberNode = Rc<RefCell<Number>>;

fn deep_copy(node: &NumberNode) -> NumberNode
{
    let node_ref = node.borrow();
    match &(*node_ref)
    {
        Number::Pair(left_node, right_node) =>
            Rc::new(RefCell::new(Number::Pair(deep_copy(left_node), deep_copy(right_node)))),
        Number::Value(number_val) => Rc::new(RefCell::new(Number::Value(*number_val)))
    }
}

fn propagate_left(node: &NumberNode, val: u8)
{
    let mut ref_mut = node.borrow_mut();
    match &mut (*ref_mut)
    {
        Number::Pair(left_node, _) => propagate_left(left_node, val),
        Number::Value(number_val) => *number_val += val
    }
}

// fn propagate_left(mut node: Rc<RefCell<Number>>, val: u8) {
//     loop {
//         let node = match &mut *node.borrow_mut() {
//             Number::Pair(left_node, _) => Rc::clone(left_node),
//             Number::Value(number_val) => {
//                 *number_val += val;
//                 return;
//             }
//         };
//     }
// }

fn propagate_right(node: &NumberNode, val: u8)
{
    let mut ref_mut = node.borrow_mut();
    match &mut (*ref_mut)
    {
        Number::Pair(_, right_node) => propagate_right(right_node, val),
        Number::Value(number_val) => *number_val += val
    }
}

fn propagate_exploded_values(left_val: u8, right_val: u8, stack: &Vec<(NumberNode, u8)>)
{
    let mut left_propagated = false;
    let mut right_propagated = false;
    for (parent_node, parent_children_visited) in stack.iter().rev()
    {
        if let Number::Pair(left_node, right_node) = &mut (*parent_node.borrow_mut())
        {
            if *parent_children_visited == 1 && !right_propagated
            {
                propagate_left(right_node, right_val);
                right_propagated = true;
            }
            else if *parent_children_visited == 2 && !left_propagated
            {
                propagate_right(left_node, left_val);
                left_propagated = true;
            }
        }
    }
}

fn explode_number(number: &mut NumberNode) -> bool
{
    let mut stack = Vec::<(NumberNode, u8)>::new();
    stack.push((number.clone(), 0));
    while let Some((number_to_visit_ref, children_visited)) = stack.pop()
    {
        let mut number_to_visit = number_to_visit_ref.borrow_mut();
        let mut do_explode = false;
        let mut left_explode_val = 0_u8;
        let mut right_explode_val = 0_u8;
        if let Number::Pair(left_ref, right_ref) = &(*number_to_visit)
        {
            let left = left_ref.borrow_mut();
            let right = right_ref.borrow_mut();
            if let (Number::Value(left_val), Number::Value(right_val)) = (&(*left), &(*right))
            {
                do_explode = stack.len() > 3;
                left_explode_val = *left_val;
                right_explode_val = *right_val;
            }
            else
            {
                if children_visited == 0
                {
                    stack.push((number_to_visit_ref.clone(), children_visited + 1));
                    stack.push((left_ref.clone(), 0));
                }
                else if children_visited == 1
                {
                    stack.push((number_to_visit_ref.clone(), children_visited + 1));
                    stack.push((right_ref.clone(), 0));
                }
            }
        }

        if do_explode
        {
            propagate_exploded_values(left_explode_val, right_explode_val, &stack);
            *number_to_visit = Number::Value(0);
            return true;
        }
    }

    false
}

fn split_number(number: &mut NumberNode) -> bool
{
    let mut ref_mut = number.borrow_mut();
    let mut pair_split = false;
    let mut is_pair = false;
    let mut val_split = false;
    let mut left_val = 0_u8;
    let mut right_val = 0_u8;
    match &mut (*ref_mut)
    {
        Number::Pair(left_node, right_node) =>
        {
            is_pair = true;
            pair_split = split_number(left_node) || split_number(right_node);
        },
        Number::Value(val) =>
        {
            if *val >= 10
            {
                val_split = true;
                left_val = *val / 2;
                right_val = (*val as f32 / 2.0).ceil() as u8;
            }
        },
    };

    if is_pair
    {
        pair_split
    }
    else if val_split
    {
        let left_node = Rc::new(RefCell::new(Number::Value(left_val)));
        let right_node = Rc::new(RefCell::new(Number::Value(right_val)));
        *ref_mut = Number::Pair(left_node, right_node);
        true
    }
    else
    {
        false
    }
}

fn add_numbers(lhs: &NumberNode, rhs: &NumberNode) -> NumberNode
{
    let mut result = Rc::new(RefCell::new(Number::Pair(deep_copy(lhs), deep_copy(rhs))));
    while explode_number(&mut result) || split_number(&mut result) { }
    result

}

fn find_split(number_str: &str) -> Option<usize>
{
    let mut bracket_count = 0;
    for (i, c) in number_str.chars().enumerate()
    {
        if c == '['
        {
            bracket_count += 1;
        }
        else if c == ']'
        {
            bracket_count -= 1;
        }
        else if c == ',' && bracket_count == 0
        {
            return Some(i);
        }
    }

    None
}

fn parse_number(number_str: &str) -> NumberNode
{
    if number_str.chars().next().unwrap() != '['
    {
        assert!(number_str.len() == 1);
        let val = number_str.chars().next().unwrap().to_digit(10).unwrap() as u8;
        Rc::new(RefCell::new(Number::Value(val)))
    }
    else
    {
        assert!(number_str.chars().last().unwrap() == ']');
        let split_index = find_split(&number_str[1..number_str.len()-1]).unwrap() + 1;
        let left_number = parse_number(&number_str[1..split_index]);
        let right_number = parse_number(&number_str[split_index+1..number_str.len()-1]);
        Rc::new(RefCell::new(Number::Pair(left_number, right_number)))
    }
}

pub fn part1(input: &str) -> String {
    let numbers = input.lines().map(parse_number).collect::<Vec<_>>();
    let first = numbers[0].clone();
    let sum = numbers.iter().skip(1).fold(first, |sum, x| add_numbers(&sum, x));
    println!("{:?}", sum.borrow());
    let magnitude = sum.borrow().magnitude();
    format!("{}", magnitude)
}

pub fn part2(input: &str) -> String {
    let numbers = input.lines().map(parse_number).collect::<Vec<_>>();
    let mut max_magnitude = 0;
    for i in 0..numbers.len()
    {
        for j in i..numbers.len()
        {
            let magnitude1 = add_numbers(&numbers[i], &numbers[j]).borrow().magnitude();
            max_magnitude = std::cmp::max(magnitude1, max_magnitude);

            let magnitude2 = add_numbers(&numbers[j], &numbers[i]).borrow().magnitude();
            max_magnitude = std::cmp::max(magnitude2, max_magnitude);
        }
    }
    format!("{}", max_magnitude)
}
