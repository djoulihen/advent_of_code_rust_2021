// <copyright file="AoC2021EditorWindow.cs" company="AMPLITUDE Studios">Copyright AMPLITUDE Studios. All rights reserved.</copyright>

using System.Runtime.InteropServices;

namespace Aoc2021
{
#if UNITY_EDITOR
    public class AoC2021EditorWindow : UnityEditor.EditorWindow
    {
        private const uint DayCount = 25;

        [System.NonSerialized]
        private string[] resultMessages = null;

        [System.NonSerialized]
        private UnityEngine.Vector2 scrollPosition;

        [UnityEditor.MenuItem("Rust/Aoc 2021")]
        private static void Init()
        {
            var window = (AoC2021EditorWindow)UnityEditor.EditorWindow.GetWindow(typeof(AoC2021EditorWindow));
            window.Show();
        }

        public void OnEnable()
        {
            this.resultMessages = new string[DayCount*2];
        }

        public void OnDisable()
        {
            this.resultMessages = null;
        }

        private void OnGUI()
        {
            this.scrollPosition = UnityEngine.GUILayout.BeginScrollView(this.scrollPosition);
            for (uint day = 0; day < DayCount; ++day)
            {
                UnityEngine.GUILayout.Space(10);
                UnityEngine.GUILayout.Label($"Day {day+1}");
                string dayStr = (day+1).ToString("00");
                string inputPath = $"../inputs/day{dayStr}.txt";

                for (byte part = 0; part < 2; ++part)
                {
                    if (UnityEngine.GUILayout.Button($"Part {part+1}"))
                    {
                        string input = System.IO.File.ReadAllText(inputPath);
                        var stringBuilder = new System.Text.StringBuilder(4096);
                        aoc_solution(day+1, part, input, stringBuilder, 4096);
                        this.resultMessages[day*2+part] = stringBuilder.ToString();
                    }

                    if (!string.IsNullOrEmpty(this.resultMessages[day*2+part]))
                    {
                        UnityEngine.GUILayout.Label(this.resultMessages[day*2+part]);
                    }
                }
            }

            UnityEngine.GUILayout.EndScrollView();
        }

        [DllImport("aoc_2021_clib", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        private static extern void aoc_solution(uint day, byte part, string input, System.Text.StringBuilder output, uint outputLength);
    }
#endif
}